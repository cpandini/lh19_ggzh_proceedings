#!/bin/bash
export PATH=".:$PATH"
export RIVET_ANALYSIS_PATH="$RIVET_ANALYSIS_PATH:../rivet-code"

python pylot.py

sed -ie 's/LINE_1/sherpa_01j_scalevar/g' undecayed_simple_config.plot
sed -ie 's/LINE_2/sherpa_lops/g' undecayed_simple_config.plot
sed -ie 's/LINE_3/Rivet_undecayed_simple_mllcut_200214_1547_mllcut/g' undecayed_simple_config.plot

sherpa_scale=0.9561177078  # 0.001852/0.001937 to account for different EW params

rivet-mkhtml --mc-errs --font times \
    sherpa_01j_scalevar.yoda:Scale=$sherpa_scale:Title='Sherpa~MEPS~0,1-jet':ErrorBands=1:ErrorBars=0:'ErrorBandColor={[RGB]{30,144,255}}':'ErrorBandOpacity=0.2':'LineColor={[RGB]{0,0,255}}' \
    sherpa_01j.yoda:Scale=$sherpa_scale:Title='Sherpa~0+1~jet':'LineColor={[RGB]{0,0,255}}' \
    sherpa_lops.yoda:Scale=$sherpa_scale:Title='Sherpa~LO+PS~0-jet':'LineColor={[RGB]{138,0,0}}' \
    Rivet_undecayed_simple_mllcut_200214_1547_mllcut.yoda:Title=POWHEG+Pythia8:'LineColor={[RGB]{34,139,34}}' \
    -c undecayed_simple_config.plot -o ../plots/rivet-plots
