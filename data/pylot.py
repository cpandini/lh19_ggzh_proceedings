#!/usr/bin/env Python

import os


tmp_plot=open('undecayed_simple_config.plot', 'w')

begin_plot = '# BEGIN PLOT '
end_plot = '# END PLOT'
legend_only = 'LegendOnly = '

with open('./tmp.plot', 'r') as old_plot_file:
    for line in old_plot_file:
        tmp_plot.write(line)
        if begin_plot in line:
            plot_name = line.replace(begin_plot, '').replace('\n','')
            tmp_plot.write(legend_only + '/' + os.getcwd() + '/LINE_1.yoda' + plot_name + ' /' + os.getcwd() + '/LINE_2.yoda' + plot_name + ' /' + os.getcwd() + '/LINE_3.yoda' + plot_name + '\n')
            if 'pT' in line:
                if 'pTVH' in line:
                    tmp_plot.write('XMax = 1000 \n' + 'Rebin = 4 \n')
                    tmp_plot.write('LegendXPos = 0.05 \n' + 'LegendYPos = 0.3 \n')
                else:
                    tmp_plot.write('XMax = 600 \n' + 'Rebin = 2 \n')
                    tmp_plot.write('LegendXPos = 0.05 \n' + 'LegendYPos = 0.3 \n')
            if 'ptbalancej3' in line:
                tmp_plot.write('XMax = 500 \n' + 'Rebin = 2 \n')
                tmp_plot.write('LegendXPos = 0.05 \n' + 'LegendYPos = 0.3 \n')
            if 'ptj3' in line:
                tmp_plot.write('XMax = 500 \n' + 'Rebin = 2 \n')
                tmp_plot.write('LegendXPos = 0.08 \n' + 'LegendYPos = 0.3 \n')
            tmp_plot.write('RatioPlotYMax = 1.8 \n' + 'RatioPlotYMin = 0.2 \n')
            if 'Delta' in line:
                tmp_plot.write('LegendXPos = 0.1 \n' + 'LegendYPos = 0.85 \n')
            if 'mVH' in line:
                tmp_plot.write('XMax=1000 \n' + 'XMin = 215 \n')
            if 'njet' in line:
                tmp_plot.write('LegendXPos = 0.05 \n' + 'LegendYPos = 0.3 \n')

tmp_plot.close()
