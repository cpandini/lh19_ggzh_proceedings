// -*- C++ -*-
#include "Rivet/Jet.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
//#include "Rivet/Tools/RivetBoost.hh"
//#include "Rivet/ParticleName.hh"
#include "Rivet/Particle.hh"
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Math/LorentzTrans.hh"
#include "Rivet/Math/Constants.hh"
#include <cmath>
#include <vector>

namespace Rivet {
    
  using namespace Cuts;
    
  class simple_HXSWG_VH_Selection : public Analysis {
  public:
        
    /// Constructor
    simple_HXSWG_VH_Selection()
      : Analysis("simple_HXSWG_VH_Selection")
    {    }


    ///////////////////////////////////////////////////////////////////
    ///
    ///                    INITIALIZATION FUNCTION
    ///
    ///////////////////////////////////////////////////////////////////
        
    // initialize all the projections
    // book all histograms
        
    void init() {           

      //---------------------------------------------------
      //             Definition of projections
      //---------------------------------------------------
            
      // Set up projections
      FinalState fs;
      addProjection(fs, "FinalState");
            
      // Cuts for electrons and muons used to build Z and W
      Cut cut_e = etaIn(-2.7,2.7) & (Cuts::pT >= 7*GeV); 
      Cut cut_m = etaIn(-2.7,2.7) & (Cuts::pT >= 7*GeV);
            
      // Gets electrons
      IdentifiedFinalState electrons(cut_e);
      electrons.acceptIdPair(PID::ELECTRON);
      addProjection(electrons, "Electrons");
      // Dressed electrons
      DressedLeptons electronClusters(fs, electrons, 0.1, /*true,*/ cut_e);
      addProjection(electronClusters, "ElectronClusters");
            
      // Gets muons
      IdentifiedFinalState muons(cut_m);
      muons.acceptIdPair(PID::MUON);
      addProjection(muons, "Muons");
      // Dressed muons
      DressedLeptons muonClusters(fs, muons, 0.1, /*true,*/ cut_m);
      addProjection(muonClusters, "MuonClusters");
            
      // Final state used as input for jet-finding.
      FinalState fs_jets(etaIn(-5,5));
      addProjection(fs_jets, "FinalState_jets");
      // Calorimeter particles : all particles but muons and neutrinos
      VetoedFinalState jet_input(fs_jets);
      jet_input.vetoNeutrinos();
      jet_input.addVetoPairId(PID::MUON);
      addProjection(jet_input, "Jet_input");
            
      // Get the jets
      addProjection(FastJets(jet_input, FastJets::ANTIKT, 0.4), "AntiKT04");
            
      // all tracks (to do deltaR with leptons)
      addProjection(ChargedFinalState(-2.5,2.5,0.5*GeV),"cfs");

      // histogram booking
      for (int nb_leptons=0; nb_leptons<=2; nb_leptons++) {
	string name;
	if(nb_leptons==0) {
	  name = "SAMPLE_0lepton_";		
	  _h_VpT_0L = bookHisto1D(name + "pTV", 200, 0, 3000);
	  _h_HpT_0L = bookHisto1D(name + "pTH", 200, 0, 3000);
	  _h_njet_0L = bookHisto1D(name + "njet", 20, 0, 20);
	  _h_mjj_0L = bookHisto1D(name + "mjj", 100, 0, 500);
	  _h_ptb1_0L = bookHisto1D(name + "ptb1", 100, 0, 1000);
	  _h_ptb2_0L = bookHisto1D(name + "ptb2", 100, 0, 1000);
	  _h_DeltaPhiVjj_0L = bookHisto1D(name + "DeltaPhiVjj", 50, 0, 3.15);
	}
	if(nb_leptons==1) {
	  name = "SAMPLE_1lepton_";
	  _h_VpT_1L = bookHisto1D(name + "pTV", 200, 0, 3000);
	  _h_HpT_1L = bookHisto1D(name + "pTH", 200, 0, 3000);	  
	  _h_njet_1L = bookHisto1D(name + "njet", 20, 0, 20);
	  _h_mjj_1L = bookHisto1D(name + "mjj", 100, 0, 500);
	  _h_ptb1_1L = bookHisto1D(name + "ptb1", 100, 0, 1000);
	  _h_ptb2_1L = bookHisto1D(name + "ptb2", 100, 0, 1000);
	  _h_DeltaPhiVjj_1L = bookHisto1D(name + "DeltaPhiVjj", 50, 0, 3.15);	  
	}
	if(nb_leptons==2) {
	  name = "SAMPLE_2lepton_";		
	  _h_VpT_2L = bookHisto1D(name + "pTV", 200, 0, 3000);
	  _h_HpT_2L = bookHisto1D(name + "pTH", 200, 0, 3000);
	  _h_njet_2L = bookHisto1D(name + "njet", 20, 0, 20);
	  _h_mjj_2L = bookHisto1D(name + "mjj", 100, 0, 500);
	  _h_ptb1_2L = bookHisto1D(name + "ptb1", 100, 0, 1000);
	  _h_ptb2_2L = bookHisto1D(name + "ptb2", 100, 0, 1000);
	  _h_DeltaPhiVjj_2L = bookHisto1D(name + "DeltaPhiVjj", 50, 0, 3.15);	  	  
	}
      }

      _h_VpT_inclusive = bookHisto1D("pTV_inclusive", 200, 0, 2000);      
      _h_nb_event = bookHisto1D("Event_type", 19, -1.5, 17.5);
      _h_nb_event_unweighted = bookHisto1D("Event_type_unweighted", 19, -1.5, 17.5);
      _h_xsect = bookHisto1D("CrossSection",3,-0.5,2.5);

      _h_mll_presel = bookHisto1D("mll_presel", 100, 0, 500);

      _h_debug_2L_1= bookHisto1D("debug_2L_1",10,-0.5,9.5);
      _h_sel_el_size = bookHisto1D("sel_el_size",10,-0.5,9.5);
      _h_chg_tracks = bookHisto1D("chg_tracks",100,0,100);
      _h_presel_el_pT = bookHisto1D("presel_el_pT", 100, 0, 500);      
      _h_presel_el_eta = bookHisto1D("presel_el_eta", 100, -5, 5);
    }
      


    ///////////////////////////////////////////////////////////////////
    ///
    ///                       ANALYSIS FUNCTION
    ///
    ///////////////////////////////////////////////////////////////////
      
    // do per-event analysis
      
    void analyze(const Event& event) {

      double weight = event.weight();
      _h_xsect->fill(1,weight);
	
      //---------------------------------------------------
      //             Creation of projections
      //---------------------------------------------------
            
      // Get the electrons (ordered by pT)
      const Particles& electrons = applyProjection<FinalState>(event, "Electrons").particlesByPt();
      const Particles& muons = applyProjection<FinalState>(event, "Muons").particlesByPt();
      // Tracks
      const Particles chg_tracks = applyProjection<ChargedFinalState>(event, "cfs").particlesByPt();            
      // Calorimeter particles
      const Particles calo_part = applyProjection<VetoedFinalState>(event, "Jet_input").particlesByPt();            
      // Visible particles (for Et miss)
      const Particles vis_part = calo_part + muons;            
      // Get the jets (ordered by pT)
      const double JETPTCUT = 10*GeV;// ATLAS standard jets collection
      const Jets jets = applyProjection<FastJets>(event, "AntiKT04").jetsByPt(JETPTCUT);
                        
      // List of all physical objects passing cuts
      Particles vectorBosons;
      Particles goodelectrons; //electrons passing cuts
      Particles goodmuons; //muons passing cuts
      Particles chosenleptons;
      Jets bjets, bjets_eta_cut, bjets_pT_cut, additionaljets;
      Jets goodjets;
      Jets signaljets;
      Jets dijet_ASJ;
      int nb_leptons; nb_leptons = -5;	    

      //---------------------------------------------------                                                                                                                                                                       
      //                     Analysis                                                                                                                                                                                             
      //---------------------------------------------------                                                                                                                                                                       

      selection(chg_tracks, electrons, muons,
		goodelectrons,
		goodmuons,
		jets, goodjets, signaljets, dijet_ASJ, 
		additionaljets, bjets, bjets_eta_cut, bjets_pT_cut);
      
      characterization(vis_part,
		       goodelectrons, goodmuons,
		       goodjets, additionaljets, bjets, signaljets, dijet_ASJ,
		       chosenleptons,
		       nb_leptons);
      
      fillhisto(vis_part, chg_tracks,
		goodjets, additionaljets, bjets, signaljets, dijet_ASJ, bjets_eta_cut, bjets_pT_cut,
		chosenleptons,
		nb_leptons,
		weight);           
    }


    //===================================================
    //                    Selection
    //===================================================

    void selection(const Particles& chg_tracks, const Particles& electrons, const Particles& muons,
		   Particles& goodelectrons,
		   Particles& goodmuons,
		   const Jets& jets, Jets& goodjets, Jets& signaljets, Jets& dijet_ASJ, 
		   Jets& additionaljets, Jets& bjets, Jets& bjets_eta_cut, Jets& bjets_pT_cut) {
      
      // Definition of all the cuts applied
      // to electrons
      const double e_pt_cut_loose = 7*GeV;
      const double e_eta_cut_loose = 2.7;
      // to muons
      const double m_pt_cut_loose = 7*GeV;
      const double m_eta_cut_loose = 2.7;
      // to central jets
      const double centraljet_pt_cut = 20*GeV;
      const double centraljet_eta_cut = 2.5;
      // to forward jets
      const double additional_jet_pt_cut = 30*GeV;
      const double additional_jet_eta_cut = 4.5;


      _h_chg_tracks->fill(chg_tracks.size());

      foreach (const Particle& m, muons) {
	if (m.pT() > m_pt_cut_loose && fabs(m.eta()) < m_eta_cut_loose) goodmuons.push_back(m);
      }
      foreach (const Particle& e, electrons) {
	_h_presel_el_pT->fill(e.pT());
	_h_presel_el_eta->fill(e.eta());
	if (e.pT() > e_pt_cut_loose && fabs(e.eta()) < e_eta_cut_loose) goodelectrons.push_back(e);
      }

      bool NoElectronInsideJet = 1; // equal to 1 if there is no lepton in jet, equal to 0 if there is lepton in jet
      bool NoMuonInsideJet = 1;
      
      // Leptons-jets overlap removal, using Loose leptons
      foreach (const Jet& jet, jets) {

	// NoElectronInsideJet = 1;
	// foreach (const Particle& e, goodelectrons) { // Overlap removal : jet/electrons
	//   const double deltaRej = deltaR(jet.momentum(), e.momentum());
	//   if (deltaRej < 0.4) {NoElectronInsideJet = 0;}
	// }	                                                                                                                                                                                          
	// NoMuonInsideJet = 1;
	// foreach (const Particle& m, goodmuons) { // Overlap removal : jet/muons
	//   const double deltaRmj = deltaR(jet.momentum(), m.momentum());
	//   if (deltaRmj < 0.4) {
	//     HepMC::GenVertex* mvertex = m.genParticle()->production_vertex();
	//     int Ntrack = 0;
	//     foreach (const Particle& c, chg_tracks) {
	//       HepMC::GenVertex* cvertex = c.genParticle()->production_vertex();
	//       if (cvertex == mvertex) {Ntrack = Ntrack + 1;}
	//     }
	//     if (Ntrack <= 3) {NoMuonInsideJet = 0;}
	//   }
	// }
              
	
	// Check if b-tagged and cuts
	if (NoElectronInsideJet && NoMuonInsideJet) {  
	  // goodjets = collection of central+forward jets
	  if ((jet.pT() > centraljet_pt_cut && fabs(jet.eta())< centraljet_eta_cut) || // pT>20GeV && |eta|<2.5 [central jets]
	      (jet.pT() > additional_jet_pt_cut && fabs(jet.eta()) < additional_jet_eta_cut)) { // pT>30GeV && |eta|<4.5 [forward jets]
	    goodjets.push_back(jet);	  
	    // signaljets = collection of central jets
	    if (jet.pT() > centraljet_pt_cut && fabs(jet.eta())< centraljet_eta_cut) { signaljets.push_back(jet); }
	    // additionaljets = collection of forward jets
	    else { additionaljets.push_back(jet); }
	    if (jet.bTagged() &&
		jet.pT() > centraljet_pt_cut && fabs(jet.eta()) < centraljet_eta_cut && bjets.size() < 2) {
	      bjets.push_back(jet);
	      dijet_ASJ.push_back(jet);
	    }
	  }       
	}
      }

      if(dijet_ASJ.size()==0 && signaljets.size()>1) {
	dijet_ASJ.push_back(signaljets[0]);
	dijet_ASJ.push_back(signaljets[1]);
      }
      if(dijet_ASJ.size()==1 && signaljets.size()>1) {
	if (dijet_ASJ[0].momentum()!=signaljets[0].momentum()) dijet_ASJ.push_back(signaljets[0]);
	else dijet_ASJ.push_back(signaljets[1]);
      }
    }

  
    //===================================================                                                                                                                                                                           
    //         Characterization of the event                                                                                                                                                                                        
    //===================================================                                                                                                                                                                           

    void characterization(const Particles& vis_part,
			  Particles& electrons, Particles& muons,
			  Jets& goodjets, Jets& additionaljets, Jets& bjets, Jets& signaljets, Jets& dijet_ASJ,
			  Particles& chosenleptons,
			  int& nb_leptons) {

      //0-leptons, 1-leptons or dileptons state
      const size_t ne = electrons.size();
      const size_t nm = muons.size();

      _h_sel_el_size->fill(ne);

      // Et miss
      FourMomentum eTmiss_m;
      foreach ( const Particle& p, vis_part ) {	eTmiss_m -= p.momentum(); }
      const double eTmiss = eTmiss_m.pT();

      // 0-leptons
      if ( (ne + nm) == 0) { nb_leptons = 0; } 

      // 1-leptons
      else if (ne == 1 && nm == 0) {
	if(eTmiss > 30*GeV) { // MET cut 30GeV (for the electron channel only)
	  nb_leptons = 1;
	  chosenleptons.push_back(electrons[0]);
	} else { nb_leptons = -1; }
      }	
      else if (ne == 0 && nm == 1) {
	nb_leptons = 1;
	chosenleptons.push_back(muons[0]);
      }
      // 2-leptons
      else if (ne == 2 && nm == 0) {
	chosenleptons.push_back(electrons[0]);
	chosenleptons.push_back(electrons[1]);
	nb_leptons = 2;
	//	if(chosenleptons.size()>1) {
	//	  const FourMomentum mll = chosenleptons[0].momentum() + chosenleptons[1].momentum();
	//	  if(mll.mass()>81*GeV && mll.mass()<101*GeV) { nb_leptons = 2; } // mll window
	//	}
      }
      else if (ne == 0 && nm == 2) {
	chosenleptons.push_back(muons[0]);
	chosenleptons.push_back(muons[1]);
	if(chosenleptons.size()>1) {
	  //	  const FourMomentum mll = chosenleptons[0].momentum() + chosenleptons[1].momentum();
	  if(chosenleptons[0].charge()*chosenleptons[1].charge()<0) { // 2 OS muons with mll window
	    nb_leptons = 2;
	    //	    if(mll.mass()>81*GeV && mll.mass()<101*GeV) { nb_leptons = 2; } // mll window
	  }
	}
      }
      else {
	nb_leptons = -3;
      }
      
      // Vector boson momentum
      FourMomentum VB_m;
      if (nb_leptons == 0) { // 0lepton
	VB_m = eTmiss_m;
      }
      if (nb_leptons == 1) { // 1lepton
	VB_m = chosenleptons[0].momentum() + eTmiss_m;
      }
      if (nb_leptons == 2) { // 2lepton
	VB_m = chosenleptons[0].momentum() + chosenleptons[1].momentum();
      }
    }

    //===================================================
    //                  Fill histograms
    //===================================================

    void fillhisto(const Particles& vis_part, const Particles& chg_tracks,
		   Jets& goodjets, Jets& additionaljets, Jets& bjets, Jets& signaljets, Jets& dijet_ASJ, Jets& bjets_eta_cut, Jets& bjets_pT_cut,
		   Particles& chosenleptons,
		   int& nb_l,
		   double& weight) {

      bool GoodEvent = 0;
      const int nb_leptons = nb_l;

      _h_nb_event->fill(0, weight);
      _h_nb_event_unweighted->fill(0);

      if (( nb_leptons == 0 || nb_leptons == 1 || nb_leptons == 2) && signaljets.size()>1){ // Signal Regions
	GoodEvent = true;
      }

      FourMomentum V_incl;

      if (nb_leptons == 2){
	_h_debug_2L_1->fill(0); // exactly 2 leptons

	FourMomentum mll = chosenleptons[0].momentum() + chosenleptons[1].momentum();
	_h_mll_presel->fill(mll.mass()/GeV, weight);

	if(mll.mass()>81*GeV && mll.mass()<101*GeV) {	  
	  _h_debug_2L_1->fill(1); // 2 leptons + mll window cut

	  V_incl = chosenleptons[0].momentum() + chosenleptons[1].momentum();
	  _h_VpT_inclusive->fill(V_incl.pT()/GeV, weight);
	}
      }  
      
      FourMomentum pTmiss;  // Missing pT
      foreach ( const Particle& c, chg_tracks ) {pTmiss -= c.momentum();}
      FourMomentum eTmiss_m; // Missing Et
      foreach ( const Particle& p, vis_part ) {eTmiss_m -= p.momentum();}
      //      const double eTmiss = eTmiss_m.pT(); 
      FourMomentum VB_m; // Vector boson momentum
      FourMomentum eTmiss_T;
      eTmiss_T=eTmiss_m;
      eTmiss_T.setPz(0);
      Jets dijet;
      Jets dijet_ordered;
      Jet thirdjet;

      // Get a jet collection of: Higgs di-b-jet pair, possibly third jet
      if(signaljets.size()>1)
	{
	  dijet.push_back(dijet_ASJ[0]);
	  dijet.push_back(dijet_ASJ[1]);
	  if(dijet_ASJ[0].pT() >= dijet_ASJ[1].pT()) {
	    dijet_ordered.push_back(dijet_ASJ[0]);
	    dijet_ordered.push_back(dijet_ASJ[1]);
	  }
	  else {
	    dijet_ordered.push_back(dijet_ASJ[1]);
	    dijet_ordered.push_back(dijet_ASJ[0]);
	  }
	}

      if(signaljets.size()>2)
	{
	  foreach (const Jet& jet, signaljets) {
	    if(jet.momentum()== dijet_ordered[0].momentum()) continue;
	    else if(jet.momentum()== dijet_ordered[1].momentum()) continue;
	    else thirdjet = jet;
	  }
	}
      

      if(GoodEvent){
	_h_nb_event->fill(1, weight);
	_h_nb_event_unweighted->fill(1);

	if (nb_leptons == 0) {VB_m = eTmiss_T;}
	if (nb_leptons == 1) {VB_m = chosenleptons[0].momentum() + eTmiss_m;}
	if (nb_leptons == 2) {VB_m = chosenleptons[0].momentum() + chosenleptons[1].momentum();}

	const FourMomentum pdijet= dijet[0].momentum() + dijet[1].momentum();
	const FourMomentum pbbj= dijet[0].momentum() + dijet[1].momentum() + thirdjet.momentum();
	//	const double deltaPhijj = deltaPhi(dijet[0].momentum(), dijet[1].momentum());
	const double deltaPhiVjj = deltaPhi(pdijet, VB_m);
	//	const double deltaEtaVjj = fabs(VB_m.eta() - pdijet.eta());
	//	const double deltaRjj = deltaR(dijet[0].momentum(), dijet[1].momentum());
	//	const double deltaEtajj = fabs(dijet[0].eta() - dijet[1].eta());
	bool Good0lepton = true;
	bool Good1lepton = true;
	bool Good2lepton = true;

	// Z(vv)H 0-leptons cuts
	if (nb_leptons != 0) Good0lepton = false;
	else if (nb_leptons == 0) {
	  //if (eTmiss < 150*GeV) {Good0lepton = false;} // MET cut
	  if(dijet_ordered[0].pT()<45*GeV) {Good0lepton = false;} // pT(b1) cut
	  if (Good0lepton == true) {
	    _h_nb_event->fill(2, weight);
	    _h_nb_event_unweighted->fill(2);
	  }
	  else { vetoEvent; }
	}

	// W(lv)H 1-lepton cut
	if (nb_leptons != 1) Good1lepton = false;
	else if(nb_leptons == 1){
	  if(dijet_ordered[0].pT()<45*GeV) {Good1lepton = false;} // pT(b1) cut
	  //if (VB_m.pT() < 150*GeV) Good1lepton = false; // WpT cut
	  if(dijet_ordered[0].pT()<45*GeV) Good1lepton = false; // pT(b1) cut
	  if (Good1lepton == true) {
	    _h_nb_event->fill(3, weight);
	    _h_nb_event_unweighted->fill(3);
	  }
	  else vetoEvent;
	}

	// Z(ll)H 2-leptons cut
	if (nb_leptons != 2) Good2lepton = false;
	else if(nb_leptons == 2) {
	  FourMomentum mll = chosenleptons[0].momentum() + chosenleptons[1].momentum();
	  if(mll.mass()<=81*GeV || mll.mass()>=101*GeV) Good2lepton = false; // mll cut
	  if(Good2lepton == true) _h_debug_2L_1->fill(2); // 2lepton + mll cut + >= 2-signal-jet
		 
	  if(dijet_ordered[0].pT()<45*GeV) Good2lepton = false; // pT(b1) cut
	  if(Good2lepton == true) _h_debug_2L_1->fill(3); // 2lepton + mll cut + >= 2-signal-jet + pT(b1) > 45GeV
	  
	  if (Good2lepton == true) {
	    _h_nb_event->fill(4, weight);
	    _h_nb_event_unweighted->fill(4);
	  }
	  else vetoEvent; 
	}


	if(Good0lepton) {
	  _h_VpT_0L->fill(VB_m.pT()/GeV, weight);
	  _h_HpT_0L->fill(pdijet.pT()/GeV, weight);	  
	  _h_njet_0L->fill(goodjets.size(), weight);
	  _h_mjj_0L->fill(pdijet.mass()/GeV, weight);
	  _h_ptb1_0L->fill(dijet[0].pT()/GeV, weight);
	  _h_ptb2_0L->fill(dijet[1].pT()/GeV, weight);
	  _h_DeltaPhiVjj_0L->fill(deltaPhiVjj, weight);
	}
	if(Good1lepton) {
	  _h_VpT_1L->fill(VB_m.pT()/GeV, weight);
	  _h_HpT_1L->fill(pdijet.pT()/GeV, weight);	  	  
	  _h_njet_1L->fill(goodjets.size(), weight);
	  _h_mjj_1L->fill(pdijet.mass()/GeV, weight);
	  _h_ptb1_1L->fill(dijet[0].pT()/GeV, weight);
	  _h_ptb2_1L->fill(dijet[1].pT()/GeV, weight);
	  _h_DeltaPhiVjj_1L->fill(deltaPhiVjj, weight);	  
	}
	if(Good2lepton) {
	  _h_VpT_2L->fill(VB_m.pT()/GeV, weight);
	  _h_HpT_2L->fill(pdijet.pT()/GeV, weight);	  	  
	  _h_njet_2L->fill(goodjets.size(), weight);
	  _h_mjj_2L->fill(pdijet.mass()/GeV, weight);
	  _h_ptb1_2L->fill(dijet[0].pT()/GeV, weight);
	  _h_ptb2_2L->fill(dijet[1].pT()/GeV, weight);
	  _h_DeltaPhiVjj_2L->fill(deltaPhiVjj, weight);	  
	}

      } else vetoEvent;
    }


    // rescaling of histograms
  void finalize() {
    _h_xsect->fill(0,crossSection());
    _h_xsect->fill(2,sumOfWeights());
        
    scale(_h_VpT_0L, crossSection()/sumOfWeights());
    scale(_h_HpT_0L, crossSection()/sumOfWeights());
    scale(_h_njet_0L, crossSection()/sumOfWeights());
    scale(_h_mjj_0L, crossSection()/sumOfWeights());
    scale(_h_ptb1_0L, crossSection()/sumOfWeights());
    scale(_h_ptb2_0L, crossSection()/sumOfWeights());
    scale(_h_DeltaPhiVjj_0L, crossSection()/sumOfWeights());

    scale(_h_VpT_1L, crossSection()/sumOfWeights());
    scale(_h_HpT_1L, crossSection()/sumOfWeights());
    scale(_h_njet_1L, crossSection()/sumOfWeights());
    scale(_h_mjj_1L, crossSection()/sumOfWeights());
    scale(_h_ptb1_1L, crossSection()/sumOfWeights());
    scale(_h_ptb2_1L, crossSection()/sumOfWeights());
    scale(_h_DeltaPhiVjj_1L, crossSection()/sumOfWeights());
    
    scale(_h_VpT_2L, crossSection()/sumOfWeights());
    scale(_h_HpT_2L, crossSection()/sumOfWeights());
    scale(_h_njet_2L, crossSection()/sumOfWeights());
    scale(_h_mjj_2L, crossSection()/sumOfWeights());
    scale(_h_ptb1_2L, crossSection()/sumOfWeights());
    scale(_h_ptb2_2L, crossSection()/sumOfWeights());
    scale(_h_DeltaPhiVjj_2L, crossSection()/sumOfWeights());

    scale(_h_VpT_inclusive, crossSection()/sumOfWeights());         
    scale(_h_mll_presel, crossSection()/sumOfWeights());
  } 
  
  private:
    Histo1DPtr _h_nb_event;
    Histo1DPtr _h_nb_event_unweighted;
    Histo1DPtr _h_xsect;
    Histo1DPtr _h_debug_2L_1;
    Histo1DPtr _h_sel_el_size;
    Histo1DPtr _h_chg_tracks;
    Histo1DPtr _h_presel_el_pT;
    Histo1DPtr _h_presel_el_eta;

    Histo1DPtr _h_mll_presel;
    
    Histo1DPtr _h_VpT_0L;
    Histo1DPtr _h_VpT_1L;
    Histo1DPtr _h_VpT_2L;
    
    Histo1DPtr _h_HpT_0L;
    Histo1DPtr _h_HpT_1L;
    Histo1DPtr _h_HpT_2L;

    Histo1DPtr _h_njet_0L;
    Histo1DPtr _h_njet_1L;
    Histo1DPtr _h_njet_2L;

    Histo1DPtr _h_mjj_0L;
    Histo1DPtr _h_mjj_1L;
    Histo1DPtr _h_mjj_2L;
    
    Histo1DPtr _h_ptb1_0L;
    Histo1DPtr _h_ptb1_1L;
    Histo1DPtr _h_ptb1_2L;

    Histo1DPtr _h_ptb2_0L;
    Histo1DPtr _h_ptb2_1L;
    Histo1DPtr _h_ptb2_2L;

    Histo1DPtr _h_DeltaPhiVjj_0L;
    Histo1DPtr _h_DeltaPhiVjj_1L;
    Histo1DPtr _h_DeltaPhiVjj_2L;

    Histo1DPtr _h_VpT_inclusive; 
    
  };
  
  DECLARE_RIVET_PLUGIN(simple_HXSWG_VH_Selection);
  
}










