%= local definitions of macros ============================
\newcommand{\Herwig}{H\protect\scalebox{0.8}{ERWIG}\xspace}
\newcommand{\Pythia}{P\protect\scalebox{0.8}{YTHIA}\xspace}
\newcommand{\POWHEGplus}{P\protect\scalebox{0.8}{OWHEG}}
\newcommand{\POWHEG}{P\protect\scalebox{0.8}{OWHEG}\xspace}
\newcommand{\Sherpa}{S\protect\scalebox{0.8}{HERPA}\xspace}
\newcommand{\Openloops}{O\protect\scalebox{0.8}{PEN}L\protect\scalebox{0.8}{OOPS}\xspace}
\newcommand{\madgraphNLO}{M\protect\scalebox{0.8}{AD}G\protect\scalebox{0.8}{RAPH}5\protect\scalebox{0.8}{\_A}MC@NLO\xspace}
\newcommand{\Rivet}{R\protect\scalebox{0.8}{IVET}\xspace}
\newcommand{\Professor}{P\protect\scalebox{0.8}{ROFESSOR}\xspace}
\newcommand{\eps}{\varepsilon}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\mb}[1]{\mathbb{#1}}
\newcommand{\tm}[1]{\scalebox{0.95}{$#1$}}
\newcommand{\qT}{$p_{\perp}$\xspace}
\newcommand{\pTH}{$p_{\perp}^{H}$\xspace}
\newcommand{\pTZ}{$p_{\perp}^{Z}$\xspace}
\newcommand{\pTZH}{$p_{\perp}^{ZH}$\xspace}
\newcommand{\pTj}{$p_{\perp}^{j}$\xspace}
\newcommand{\njet}{$N_{\text{jets}}$\xspace}
\newcommand{\DPhiVH}{$\Delta \phi (Z, H)$\xspace}
\newcommand{\muR}{$\mu_\text{R}$\xspace}
\newcommand{\muF}{$\mu_\text{F}$\xspace}



%= title + authors =====================================
\section{A study of loop-induced $ZH$ production with up to one additional jet ~\protect\footnote{
  E.~Bothmann, 
  M.~Calvetti,
  P.~Francavilla,
  C.~Pandini,
  E.~Re,
  S.~L.~Villani
  }{}}

%= MANDATORY label ======================================
\label{sec:wghiggs1zh}

%= (optional) preamble ================================== 
%\begin{abstract}
%If you want, here write an abstract or a preamble to your contribution.
%\end{abstract}

%= content ===== ========================================
\subsection{Introduction}
\label{sec:wghiggs1zh:introduction}
The $gg \rightarrow ZH$ production can be considered as a standalone process whose LO QCD contributions start at $\mathcal{O}(\alpha_S^2)$, corresponding to the Feynman diagrams in Fig.~\ref{fig:wghiggs1zh:LOdiagrams}. Calculations for the inclusive cross-section at LO QCD are available and are characterized by the destructive interference between the box and triangle diagrams~\cite{Englert:2013vua}. The two initial-state gluons lead to a rather strong renormalization and factorization scale dependence of about 30\,\%~\cite{deFlorian:2016spz}, thus increasing the theoretical uncertainty of $ZH$ relative to $WH$ production, where the gluonic channel does not contribute at LO. Experience from the gluon-fusion process $gg \rightarrow H$ (which has the same initial state and color structure as $gg \rightarrow ZH$) shows, however, that the LO scale uncertainty drastically underestimates the actual size of the higher-order corrections.

The NLO QCD $\mathcal{O}(\alpha_S^3)$ corrections to this process are beyond the technology currently available, due to the presence of massive multi-scale double-box integrals. However, a NLO perturbative correction factor $k_\text{NLO} = \sigma_\text{NLO} / \sigma_\text{LO}$ can be calculated in the limit of an infinite top quark mass and a vanishing bottom quark mass, known as the `effective field theory' NLO(EFT) approach~\cite{Altenkamp:2012sx}. The validity of this approximation holds well for $m_H=125$\,GeV at the center of mass energies considered at the LHC (from 7 to 13\,TeV), but of course worsens for larger $\sqrt{s}$ and in specific kinematic regimes (for instance in the boosted Higgs regime). The calculation yields $k_\text{NLO} \approx 2$ for $m_H=125$\,GeV, which is indeed not covered by the size of the LO scale uncertainty. The impact of a threshold resummed cross-section for $gg \rightarrow ZH$ at NLL has been considered~\cite{Harlander:2014wda}, matched to the NLO(EFT) result: the central value of the inclusive $\sigma^{ggZH}$ cross-section increases by 18\,\% at $\sqrt{s} = 13$\,TeV, while the uncertainty from scale variations decreases by a factor of three to four.
%As we could \todo{why?} expect the uncertainty from renormalization and factorization scale variations on the NLO result is decreased approximately by a factor of two, compared to the LO case. 

The NLO(EFT)+NLL $\sigma^{ggZH}$ contribution to the total $\sigma^{ZH}$ cross-section is of the order of 14\,\% at $\sqrt{s}=13$\,TeV, an already sizable contribution that becomes even more pronounced for large transverse momenta of the Higgs boson \pTH. This is a consequence of the threshold effect from the presence of top quark loops which makes the gluon-induced process especially important for \pTH$  \gtrsim m_{\text{top}}$, with a transverse momentum spectrum fundamentally different from the dominant quark-initiated contribution. Furthermore, the process has a peculiar sensitivity to new physics: through modified Higgs coupling to SM states, through new heavy colored states participating in the loops or through new $s$-channel pseudoscalars proposed in SM extensions.

A precise modeling of this process is thus key for the experimental analyses of LHC data performed by ATLAS and CMS targeting $VH$ final states. Both collaborations have so far relied on \POWHEG~\cite{Nason:2004rx, Frixione:2007vw, Alioli:2010xd} matched to the \Pythia~8~\cite{Sjostrand:2014zea} parton-shower (PS) to simulate these events at LO+PS, scaling the LO total cross-section to the state-of-the-art calculation at NLO(EFT)+NLL~\cite{deFlorian:2016spz}. While this approach allows to consider the normalization effect of the available higher-order corrections, we highlight that the modeling of differential distribution is included only at LO, and the perturbative QCD uncertainties induced by analysis selections and cuts (e.g. jet-vetoes, \pTH~cuts, etc.) fully rely on the LO+PS simulation.
While a full NLO calculation with finite top-mass effects remains to be performed, different Monte Carlo tools have been developed to allow the LO+PS simulation of $2\rightarrow3$ matrix elements for loop-induced $ZH$+jet processes, to obtain merged multi-leg samples of 0- and 1-jet multiplicities~\cite{Goncalves:2015mfa, Hespel:2015zea}. In this study we consider the impact of the simulation of higher jet multiplicities at matrix element level, by comparing the modeling of $gg \rightarrow ZH$ provided by the \Sherpa event generator with the \POWHEGplus+\Pythia setup normally used by the experimental analyses.
\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=5cm]{plots/gghz1lbox2.pdf}
    \hspace{1cm}
    \includegraphics[width=5cm]{plots/gghz1ltriangle.pdf}
    \caption{Representative Feynman diagrams that contribute to $gg \to ZH$ at LO.}
    \label{fig:wghiggs1zh:LOdiagrams}
  \end{center}
\end{figure}

\subsection{Monte Carlo setup}
\label{sec:wghiggs1zh:MCsetup}
We consider different Monte Carlo (MC) tools providing LO predictions for the $gg\rightarrow ZH$ process with finite top-quark mass effects.
For the scope of this study we only consider the leptonic decay channel of the
Z boson to electrons for simplicity.
The decay is done on the matrix-element level, and hence finite-width effects and spin correlations are accounted for in the simulation. The Higgs is left undecayed. The simulation of multiple-parton interactions and hadronisation effects is disabled. The emission of additional QED radiation and other higher-order QED corrections are also turned off.
Only $gg \rightarrow Z(\rightarrow e^+e^-)H$ processes are thus considered in the following sections, evaluated
for proton-proton collisions at a centre-of-mass energy of $\sqrt s =
13\,\text{TeV}$.
The factorisation and the renormalisation scale are both set to
\[
   \mu_\text{F}=\mu_\text{R}=\hat{H}_{\perp}=\sqrt{M_{H}^{2}+{p_{\perp}^{H}}^{2}}+\sum_{i} p_{\perp}^{(i)} 
   ,\]
with the sum running over the two leptons. For some of the results, we show an envelope over 7-point variations of the renormalisation and factorisation scale, i.e.\ varying both scales independently by factors of two and one half, but omitting those where the two scale variation factors differ by a factor of 4. The shower starting scale is set to the invariant mass of the final-state system if not otherwise mentioned.
For the proton structure functions, the PDF4LHC15 set is used at NLO
accuracy~\cite{Butterworth:2015oua}.
The top-quark mass and its width are set to $m_t=172.5$\,GeV
and $\Gamma_t=1.32$\,GeV, respectively,
and the bottom-quark mass is given by $m_b=4.95$\,GeV.
Furthermore, we have $\alpha_\text{QED}=1/132.5070$.
The boson masses are $m_W=80.419$\,GeV, $m_Z=91.188$\,GeV and $m_H=125$\,GeV.
% \Gamma_Z=2.4410000000
%Finite-width effects are taken into account using the complex-mass scheme~\cite{Denner:1999gp,Denner:2005fg}.

\subsubsection{\Sherpa}
\label{subsec:wghiggs1zh:SHERPA}
The first set of results is obtained with the \Sherpa event generator
v.2.2.8~\cite{Gleisberg:2008ta,Bothmann:2019yzt}, interfaced to \Openloops~2~\cite{Buccioni:2019sur} to provide the loop contributions.
%For the $H$ decay, a narrow-width factorisation is employed:
%A separate decay matrix element is used for $H \to b \bar b$.
%The decay kinematics are adjusted a-posteriori to a Breit-Wigner distribution
%to account for the finite $H$ width.
%Spin correlations are not taken into account.
The fixed-order sample is evolved further by the CS parton
shower~\cite{Schumann:2007mg}, which is the default shower implemented in \Sherpa.
For combining 0-jet and 1-jet samples into a single inclusive sample the
multi-jet merging at leading order implemented in \Sherpa is
used~\cite{Catani:2001cc,Hoeche:2009rj}, adapted for loop-induced processes
in~\cite{Cascioli:2013gfa}.
Factorisation and renormalisation scale variations are calculated on-the-fly~\cite{Bothmann:2016nao}.
In addition, we vary the shower starting scale up and down by factors of $\sqrt
2$ for the LO+PS sample, to give an estimate of the resummation uncertainty.
For the multijet-merged sample, we choose a merging cut of $Q_\text{cut}=20$\,GeV. This technical cut separates the phase-space regions populated by the matrix elements and the parton shower. Variations of this cut by a factor of 2, i.e.\ $Q_\text{cut}=10$\,GeV and $40$\,GeV, are also studied.
With the above specifications, we generate two \Sherpa samples:
\begin{itemize}
   \item loop-induced $gg \rightarrow Z(\rightarrow e^+e^-)H$ + 0-jet (LO+PS)
   \item loop-induced $gg \rightarrow Z(\rightarrow e^+e^-)H$ + 0,1-jets at LO with multijet-merging (MEPS 0,1j)
\end{itemize}
Note that the 1-jet matrix elements also include diagrams with initial-state quarks, cf.\ e.g.\ \cite{Hespel:2015zea,Goncalves:2015mfa}.
The corresponding squared loop amplitudes
form a finite and gauge-invariant subset of the NNLO corrections
for the $pp \to ZHj$ process.

\subsubsection{\POWHEGplus+\Pythia}
\label{subsec:wghiggs1zh:PP8}
The second set of results relies on the \POWHEG \texttt{ggHZ} event generator code to obtain an inclusive $gg \rightarrow Z(\rightarrow e^+e^-)H$ + 0-jet LO sample, interfaced to the \Pythia~8.2 parton-shower algorithm. The NNPDF3.0 PDF~\cite{Ball:2014uwa} set is used within the shower.
The PS matching relies on the \Pythia~8 `wimpy-shower' algorithm, in which the shower starting scale is set to the invariant mass of the $ZH$ system $m_{ZH}$, letting \Pythia~8 continue the showering process at the hardness scale at which \POWHEG leaves off. An alternative approach is considered, to assess the impact of the matching scheme on the results, by adopting the \POWHEG-specific \texttt{main31} \Pythia algorithm for a vetoed `power-shower', in which the shower starting scale is set to the kinematical limit ($\pT = \sqrt{\hat{s}}/2$), combined with an a-posteriori veto of emissions already covered by \POWHEG.
%Events are simulated at `parton-level', by switching off the multiple-parton-interaction (MPI) simulation and halting the generation after the parton-level activity of the hard process in the \Pythia8 algorithm (\texttt{HadronLevel:all = off}, \texttt{PartonLevel:MPI = off}).


\subsection{Analysis and results}
\label{sec:wghiggs1zh:rivetresults}
The analysis selection applied for this study
has been implemented using the \Rivet 2 analysis
framework~\cite{Buckley:2010ar} and it is designed to obtain the simplest possible selection for the $gg \rightarrow Z(\rightarrow e^+e^-)H$ channel. Lepton and jet \qT and $\eta$ cuts are defined to be close to the ranges probed by experimental analyses~\cite{Aaboud:2018zhk}.
The event selection requires exactly 2 electrons with \qT$ > 7$ GeV and $|\eta|<2.7$, within a lepton-pair invariant mass window of $81$\,GeV $< m_{\ell\ell}< 101$\,GeV, in addition to exactly one undecayed Higgs candidate.
Jets are reconstructed and selected only to study their multiplicity and transverse momentum distribution, i.e.\ no event selection or veto is applied based on the jet activity. The reconstruction is done using the anti-$k_\text T$ clustering algorithm with a jet-radius parameter of $R=0.4$. The jet transverse momentum is required to be greater than $25$\,GeV, with $|\eta|<4.5$. Jet candidates are discarded if an electron is found within a cone of $\Delta R < 0.4$ around the jet axis. There is no requirement on the jet flavor.

\subsubsection{Total cross-sections}
\label{subsec:wghiggs1zh:xs}
We start by discussing the cross-section of the samples considered in this study. In Tab.~\ref{tab:wghiggs1zh:xs} we show the total cross-section obtained from the three setups, before and after the analysis selection detailed in Sec.~\ref{sec:wghiggs1zh:rivetresults}. The total cross-section is computed at LO in QCD in all cases, and it includes the branching ratio of the $Z$ boson decay to electrons. The uncertainty on the total cross-section comes from the 7-point ($\mu_R$, $\mu_F$) scale variations described in Sec.~\ref{sec:wghiggs1zh:MCsetup}. The total cross-section for the \Sherpa MEPS 0,1-jets setup includes a cut on the di-electron invariant mass of $m_{\ell\ell} > 66$\,GeV, to exclude the contribution of photon-mediated diagrams, which are not included in the other 0-jet LO calculations. The total cross-sections and their QCD uncertainties are well consistent with the $gg\rightarrow ZH$ calculations documented in the literature~\cite{deFlorian:2016spz}. The fiducial cross-sections show that the analysis selection has an acceptance of $85$--$90$\,\%, consistently among the three setups.

% NOTE: the after-cuts numbers can be generated using the data/xs script 
\begin{table}[htbp]
\caption[]{Total and fiducial $gg \rightarrow ZH$ cross-sections from the \POWHEGplus+\Pythia and \Sherpa 0-jet inclusive LO setups, and the \Sherpa MEPS 0,1-jets setup. The uncertainty quoted on the total cross-section is obtained from the 7-point ($\mu_R$, $\mu_F$) QCD scale variations.}
\begin{center}
\begin{tabular}{@{}llll@{}}
\toprule
    Cross-section [pb]        & \POWHEGplus+\Pythia & \Sherpa LO+PS &  \Sherpa MEPS 0,1j  \\ \midrule
total cross-section (before cuts) &0.001883(1)$^{+25\%}_{-19\%}$   &    0.001852(4)$^{+25\%}_{-19\%}$  & 0.001640(1)$^{+39\%}_{-26\%}$ \\
fiducial cross-section (after cuts)  &0.001575(3)  &    0.001661(3)  &   0.001384(1)  \\
\bottomrule
\end{tabular}
\end{center}
\label{tab:wghiggs1zh:xs} 
\end{table}



\subsubsection{Differential distributions}
\label{sec:wghiggs1zh:distributions}
We focus on the study of a few key observables to highlight the impact of the 0,1-jets merged setup compared to the inclusive 0-jet LO+PS ones, namely:
\begin{itemize}
  \item \pTZH, the transverse momentum of the $ZH$ boson-pair,
  \item \DPhiVH, the azimuthal angular distance between the $Z$ and the Higgs boson candidates,
  \item \pTH and \pTZ, the transverse momenta of the Higgs boson and $Z$ boson candidates,
  \item \njet, the total number of jet with \qT$>25$ GeV and $|\eta|<4.5$ (in exclusive bins),
  \item \pTj, the transverse momentum of the leading jet.
\end{itemize}
Figures~\ref{fig:wghiggs1zh:plot1}--\ref{fig:wghiggs1zh:plot3} show the
comparison between the \Sherpa and \POWHEGplus+\Pythia samples for these variables. All distributions are normalized to the fiducial cross-section predicted by the respective tools and reported in Tab.~\ref{tab:wghiggs1zh:xs}. The uncertainty band from 7-point QCD scale variations is shown only for the \Sherpa 0,1-jets MEPS prediction. However, it is found to be very consistent for both 0-jet LO+PS samples. The QCD uncertainty is in all cases fairly flat across the differential distributions. We also note that the merging cut $Q_\text{cut}$ variations considered for the MEPS 0,1-jets setup have a negligible impact for the distributions under study, compared to the QCD scale variations, and are thus not displayed.
First, we notice that the \Sherpa and \POWHEGplus+\Pythia 0-jet LO+PS distributions show a reasonable level of agreement, with \POWHEGplus+\Pythia predicting slightly harder \qT spectra and a lower jet-multiplicity (note again that for these LO samples the emission of extra QCD radiation is only modeled by the parton-shower algorithms). Both 0-jet LO+PS predictions have very similar behavior when compared to the MEPS 0,1-jet \Sherpa setup.

In Fig.~\ref{fig:wghiggs1zh:plot1} we observe that the transverse momentum of the $ZH$ system \pTZH shows interesting features: up to \pTZH $\sim 400$\,GeV the 0-jet prediction is enhanced, while above this threshold the MEPS 0,1-jets sample predicts a harder \qT spectrum. Event topologies with hard QCD radiation recoiling against the $ZH$ system, which are modeled by the \Sherpa MEPS 0,1-jets matrix-element, become dominant in the high-\pTZH regime. Both \Sherpa and \POWHEGplus+\Pythia 0-jet LO+PS predictions fail to capture the harder tail of the $ZH$ \qT spectrum when compared to the MEPS 0,1-jets. 
Considering the azimuthal distance between the Higgs and $Z$ bosons \DPhiVH, we observe that the \Sherpa 0-jet LO+PS prediction seems to lead to slightly softer QCD radiation, resulting in an enhancement of event topologies where the Higgs and $Z$ bosons are produced back-to-back (\DPhiVH$\sim \pi$), while \POWHEGplus+\Pythia and \Sherpa MEPS 0,1-jets show reasonable agreement in this region. We also notice that the MEPS 0,1-jets setup does not predict a large enhancement of collinear $ZH$ topologies, which suggests that the recoil from the hard QCD emission modeled by the \Sherpa matrix-element is mainly captured by one of the bosons, with the other one remaining relatively soft.

This consideration is further supported by the Higgs and $Z$ transverse momentum distributions shown in Fig.~\ref{fig:wghiggs1zh:plot2}: we notice indeed that for the MEPS 0,1-jets setup the \pTH becomes harder above 350--400\,GeV, while an opposite behavior is found for the \pTZ, which is consistently softer compared to the 0-jet setups from both \Sherpa and \POWHEGplus+\Pythia. This supports the conclusion that when including the matrix-element description of extra QCD radiation, the dominant topology for \qT$ > 400$\,GeV includes a hard QCD jet mainly recoiling against the Higgs, with the production of a softer $Z$ boson. This behavior was also reported in~\cite{Hespel:2015zea}. We observe that both 0-jet LO+PS predictions do not properly model this feature.

In Fig.~\ref{fig:wghiggs1zh:plot3} we show the number of hadronic jets \njet, in exclusive bins, and the transverse momentum of the hardest selected jet \pTj: we notice that both 0-jet LO+PS tools are in good agreement in the 0- and 1-jet bins, with larger discrepancies for higher jet-multiplicities. We highlight that the \textit{exclusive} \njet distribution is by construction affected by the parton-shower modeling in all bins (including the 0-jet bin) for a LO+PS prediction. The \qT  of the extra QCD radiation shows a very similar behavior as the transverse momentum of the $ZH$ pair, as expected. 

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{plots/rivet-plots/undecayed_simple_ZllH/SAMPLE_2lepton_pTVH.pdf}
\includegraphics[width=0.49\textwidth]{plots/rivet-plots/undecayed_simple_ZllH/SAMPLE_2lepton_DeltaPhiVjj.pdf}
\caption{Comparison of the 0-jet inclusive and 0,1-jets merged setups from \Sherpa and \POWHEGplus+\Pythia for the transverse momentum of the $ZH$ pair \pTZH, and the azimuthal separation between the bosons \DPhiVH. The blue error band represents the (\muR, \muF) QCD scale variations for the \Sherpa 0,1-jets sample.}
\label{fig:wghiggs1zh:plot1} 
\end{center}
\end{figure} 

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{plots/rivet-plots/undecayed_simple_ZllH/SAMPLE_2lepton_pTH.pdf}
\includegraphics[width=0.49\textwidth]{plots/rivet-plots/undecayed_simple_ZllH/SAMPLE_2lepton_pTV.pdf}
\caption{Comparison of the 0-jet inclusive and 0,1-jets merged setups from \Sherpa and \POWHEGplus+\Pythia for the transverse momenta of the Higgs and the $Z$ boson, \pTH and \pTZ. The blue error band represents the (\muR, \muF) QCD scale variations for the \Sherpa samples.}
\label{fig:wghiggs1zh:plot2} 
\end{center}
\end{figure} 

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{plots/rivet-plots/undecayed_simple_ZllH/SAMPLE_2lepton_njet.pdf}
\includegraphics[width=0.49\textwidth]{plots/rivet-plots/undecayed_simple_ZllH/SAMPLE_2lepton_ptj3.pdf}
\caption{Comparison of the 0-jet inclusive and 0,1-jets merged setups from \Sherpa and \POWHEGplus+\Pythia for the total number of hadronic jet \njet (in exclusive bins), and the transverse momentum of the leading jet \pTj. The blue error band represents the (\muR, \muF) QCD scale variations for the \Sherpa 0,1-jets sample.}
\label{fig:wghiggs1zh:plot3} 
\end{center}
\end{figure} 

\subsubsection{Parton shower and matching variations}
\label{sec:wghiggs1zh:ps}
While being far from a robust definition of a `parton-shower uncertainty', we investigate the effect of the parton-shower setup and matching for the $gg \rightarrow ZH$ process. Relying on LO MC predictions we can expect parton-shower effects to be sizeable, since a large part of the phase-space will be populated by the shower algorithm.
In order to assess the effect of parton-shower variations for this process, we compare the alternative \POWHEGplus+\Pythia setups for wimpy and vetoed power shower introduced in Sec.~\ref{subsec:wghiggs1zh:PP8}, to the \Sherpa 0-jet inclusive LO sample with variations of the shower starting scale by a factor of $\sqrt{2}$ around its central value of $m_{ZH}$, introduced in Sec.~\ref{subsec:wghiggs1zh:SHERPA}. The \Sherpa MEPS 0,1-jets sample is not considered in this study.  We show this comparison for four observables, which capture interesting effects for this LO process: \pTZH, \pTj, \DPhiVH and \njet.
From the transverse momentum distributions shown in Fig.~\ref{fig:wghiggs1zh:plot4} for the $ZH$ pair (top left) and the hardest QCD radiation (top right), we can observe how the shower starting scale variations on the \Sherpa prediction lead to a large effect when entering the regime dominated by hard QCD emission, for \qT larger than 200--300\,GeV. A similar behavior is observed for the azimuthal separation \DPhiVH (bottom left), with large shower starting scale variations away from the back-to-back $ZH$ peak.
As highlighted in Sec.~\ref{sec:wghiggs1zh:distributions} the 0-jet LO samples fully rely on the parton-shower in this region, and fail to reproduce the 0,1-jets MEPS prediction: the large sensitivity to the choice of shower starting scale further supports the conclusion that 0-jet LO predictions are not suited to model the $gg\rightarrow ZH$ process in this part of the phase space, and the need for a merged sample.	
We also note that the shower starting scale variation band, for \qT$> 300$--$400 $\,GeV,  becomes more important than the QCD perturbative uncertainty estimated from ($\mu_R$, $\mu_F$) scale variations (shown in Fig.~\ref{fig:wghiggs1zh:plot1}) which is relatively flat across the \qT spectrum.

From the comparison of the \POWHEGplus+\Pythia wimpy and  vetoed power shower algorithms we do not observe striking differences: the wimpy shower prediction is consistently slightly harder for the \qT distributions and features larger jet multiplicities. We observe that differences between the \POWHEGplus+\Pythia matching algorithms are found to be sub-dominant compared to the shower scale variation effect studied in \Sherpa. We note that the discrepancy between the \POWHEGplus+\Pythia and \Sherpa 0-jet LO \pTZH predictions is covered by the shower starting scale variation band, while this is not the case for the \njet distribution, where scale variations have a more modest effect.  
In conclusion the study of the impact of parton-shower variations for the $gg\rightarrow ZH$ process further supports the importance of a merged 0,1-jets prediction for the modeling of the high \qT regime. This observation, together with the impact of the 0,1-jets MEPS prediction on the size of shower starting scale variation, is confirmed in~\cite{Hespel:2015zea}.


\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{plots/showers/undecayed_simple_ZllH/SAMPLE_2lepton_pTVH.pdf}
\includegraphics[width=0.49\textwidth]{plots/showers/undecayed_simple_ZllH/SAMPLE_2lepton_ptj3.pdf}\\
\includegraphics[width=0.49\textwidth]{plots/showers/undecayed_simple_ZllH/SAMPLE_2lepton_DeltaPhiVjj.pdf}
\includegraphics[width=0.49\textwidth]{plots/showers/undecayed_simple_ZllH/SAMPLE_2lepton_njet.pdf}
\caption{Comparison of the 0-jet inclusive distribution for the \POWHEG+\Pythia8 wimpy and vetoed power shower setups and the \Sherpa prediction, with shower starting scale variations band shown in blue, for the transverse momentum of the $ZH$ pair \pTZH, the total number of hadronic jet \njet, and the transverse momentum of the leading jet \pTj.}
\label{fig:wghiggs1zh:plot4} 
\end{center}
\end{figure} 

\subsection{Conclusions}
\label{sec:wghiggs1zh:conclusions}
We present a study of the MC modeling of the $gg \rightarrow ZH$ process in the typical regions explored by the experimental analyses at the LHC, highlighting its characteristic features from the total cross-section to some of the main differential observables. We focus on the comparison between different LO+PS tools with respect to the improved merged MEPS 0,1-jets prediction from the \Sherpa generator. We observe that in the high transverse momentum regime (\qT$\gtrsim 300$\,GeV) the inclusion of $2\rightarrow 3$ matrix elements in the MEPS setup leads to a more accurate modeling: interestingly we note that the dominant event topology in this region includes a hard QCD radiation recoiling against an high-\qT Higgs, with a softer $Z$ emitted at high \DPhiVH angle. This topology is not properly modeled by the 0-jet LO+PS tools considered. The \qT asymmetry between the Higgs and $Z$ boson is a distinctive feature which might be considered for future studies to provide a better characterization of $gg \rightarrow ZH$ processes. From the study of parton-shower variations we observe a large sensitivity to the choice of shower starting scale for the 0-jet LO+PS setup, which further support the choice of a more accurate MEPS 0,1-jets prediction, expected to strongly reduce the shower starting scale dependence.
While the study of different predictions from \POWHEGplus+\Pythia and \Sherpa provides robustness to these results, we remark that the comparison to an alternative 0,1-jets merged tool (for instance the \madgraphNLO prediction~\cite{Hespel:2015zea}) would provide more insight on the feature of the $gg \rightarrow ZH$ $2\rightarrow 3$ matrix elements, and we leave this for further studies.

\subsection{Acknowledgements}
CP acknowledges support by the CERN EP Department.
\label{sec:wghiggs1zh:acknowledgements}


%= undefine macros (MANDATORY) ====================
\let\Herwig\undefined
\let\Pythia\undefined
\let\POWHEGplus\undefined
\let\POWHEG\undefined
\let\Sherpa\undefined
\let\Openloops\undefined
\let\Rivet\undefined
\let\madgraphNLO\undefined
\let\Professor\undefined
\let\eps\undefined
\let\mc\undefined
\let\mr\undefined
\let\mb\undefined
\let\tm\undefined
\let\qT\undefined
\let\pTH\undefined
\let\pTZ\undefined
\let\pTZH\undefined
\let\pTj\undefined
\let\njet\undefined
\let\DPhiVH\undefined
\let\muR\undefined
\let\muF\undefined



