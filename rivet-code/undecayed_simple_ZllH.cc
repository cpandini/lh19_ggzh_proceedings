// -*- C++ -*-
#include "Rivet/Jet.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
//#include "Rivet/Tools/RivetBoost.hh"
//#include "Rivet/ParticleName.hh"
#include "Rivet/Particle.hh"
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Math/LorentzTrans.hh"
#include "Rivet/Math/Constants.hh"
#include <cmath>
#include <vector>

namespace Rivet {
    
  using namespace Cuts;
    
  class undecayed_simple_ZllH : public Analysis {
  public:
        
    undecayed_simple_ZllH()
      : Analysis("undecayed_simple_ZllH")
    {    }
        


    void init() {           

      FinalState fs;
      addProjection(fs, "FinalState");

      // identified Higgs (only used for Sherpa)
      IdentifiedFinalState hfinder(fs);
      hfinder.acceptIdPair(25);
      declare(hfinder, "HFinder");

      Cut cut_e = etaIn(-2.7,2.7) & (Cuts::pT >= 7*GeV);            
      IdentifiedFinalState electrons(cut_e);
      electrons.acceptIdPair(PID::ELECTRON);
      addProjection(electrons, "Electrons");
                        
      // Final state used as input for jet-finding.
      FinalState fs_jets(etaIn(-5,5));
      addProjection(fs_jets, "FinalState_jets");
      // Calorimeter particles : all particles but muons and neutrinos
      VetoedFinalState jet_input(fs_jets);
      jet_input.vetoNeutrinos();
      jet_input.addVetoPairId(PID::MUON);
      //jet_input.addDecayProductsVeto(PID::HIGGS);
      addProjection(jet_input, "Jet_input");            
      addProjection(FastJets(jet_input, FastJets::ANTIKT, 0.4), "AntiKT04");            

      // histogram booking
      string name;
      name = "SAMPLE_2lepton_";		
      _h_njet_2L = bookHisto1D(name + "njet", 20, 0, 20);

      _h_VpT_2L = bookHisto1D(name + "pTV", 200, 0, 3000);
      _h_pTVH_2L = bookHisto1D(name + "pTVH", 300, 0, 1500);
      _h_mVH_2L = bookHisto1D(name + "mVH", 200, 0, 1000);            
      _h_HpT_2L = bookHisto1D(name + "pTH", 200, 0, 3000);
      _h_DeltaPhiVjj_2L = bookHisto1D(name + "DeltaPhiVjj", 50, 0, 3.15);	  	  
      
      _h_VpT_2L_0J = bookHisto1D(name + "0J_pTV", 200, 0, 3000);
      _h_pTVH_2L_0J = bookHisto1D(name + "0J_pTVH", 300, 0, 1500);
      _h_mVH_2L_0J = bookHisto1D(name + "0J_mVH", 200, 0, 1000);
      _h_HpT_2L_0J = bookHisto1D(name + "0J_pTH", 200, 0, 3000);
      _h_DeltaPhiVjj_2L_0J = bookHisto1D(name + "0J_DeltaPhiVjj", 50, 0, 3.15);      
     
      _h_VpT_2L_1pJ = bookHisto1D(name + "1pJ_pTV", 200, 0, 3000);
      _h_pTVH_2L_1pJ = bookHisto1D(name + "1pJ_pTVH", 300, 0, 1500);
      _h_mVH_2L_1pJ = bookHisto1D(name + "1pJ_mVH", 200, 0, 1000);
      _h_HpT_2L_1pJ = bookHisto1D(name + "1pJ_pTH", 200, 0, 3000);
      _h_DeltaPhiVjj_2L_1pJ = bookHisto1D(name + "1pJ_DeltaPhiVjj", 50, 0, 3.15);
      
      _h_DeltaPhiVHj3_2L = bookHisto1D(name + "DeltaPhiVHj3", 50, 0, 3.15);	  	  
      _h_ptj3_2L = bookHisto1D(name + "ptj3", 100, 0, 1000);
      _h_ptbalancej3_2L = bookHisto1D(name + "ptbalancej3", 100, 0, 1000);
      _h_ptbalancej3_2L_1J = bookHisto1D(name + "ptbalancej3_1J", 100, 0, 1000);
      _h_ptbalancej3_2L_2pJ = bookHisto1D(name + "ptbalancej3_2pJ", 100, 0, 1000);
           
      _h_VpT_inclusive = bookHisto1D("pTV_inclusive", 200, 0, 2000);      
      _h_mll_presel = bookHisto1D("mll_presel", 100, 0, 300);
      _h_mll = bookHisto1D("mll", 100, 0, 300);

      _h_VpT_inclusive_0J = bookHisto1D("pTV_inclusive_0J", 200, 0, 2000);
      _h_mll_presel_0J = bookHisto1D("mll_presel_0J", 100, 0, 300);

      _h_VpT_inclusive_1pJ = bookHisto1D("pTV_inclusive_1pJ", 200, 0, 2000);
      _h_mll_presel_1pJ = bookHisto1D("mll_presel_1pJ", 100, 0, 300);

      _h_xsect = bookHisto1D("CrossSection",3,-0.5,2.5);
      
    }
    


    ///////////////////////////////////////////////////////////////////
    ///
    ///                       ANALYSIS FUNCTION
    ///
    ///////////////////////////////////////////////////////////////////
      
    // do per-event analysis
      
    void analyze(const Event& event) {
      double weight = event.weight();
      _h_xsect->fill(1,weight);
	

      // Find undecayed Higgs particle
      Particle Higgs;
      GenVertex *HSvtx = event.genEvent()->signal_process_vertex();
      int Nhiggs=0;
      for ( const GenParticle *ptcl : Rivet::particles(event.genEvent()) ) {

        // a) Reject all non-Higgs particles
	if ( !PID::isHiggs(ptcl->pdg_id()) ) continue;
        // b) select only the final Higgs boson copy, prior to decay
        if ( ptcl->end_vertex() && !hasChild(ptcl,PID::HIGGS) ) {
	  Higgs = ptcl; ++Nhiggs;
        }
        // c) if HepMC::signal_proces_vertex is missing
	//    set hard-scatter vertex based on first Higgs boson
        if ( HSvtx==nullptr && ptcl->production_vertex() && !hasParent(ptcl,PID::HIGGS) )
          HSvtx = ptcl->production_vertex();
      }

      if (HSvtx == nullptr || Nhiggs != 1) {
        // assume we are analysing Sherpa events
        const IdentifiedFinalState& hfinder = apply<IdentifiedFinalState>(event, "HFinder");
        Particles hbosons = hfinder.particles();
        Nhiggs = hbosons.size();
        if (Nhiggs!=1) {
          vetoEvent;
        }
        Higgs = hbosons[0];
      } else {
        if (HSvtx == nullptr) vetoEvent;
        if (Nhiggs!=1) vetoEvent;
      }
                  
      const Particles& electrons = applyProjection<FinalState>(event, "Electrons").particlesByPt();
      const double JETPTCUT = 10*GeV;// ATLAS standard jets collection
      const Jets jets = applyProjection<FastJets>(event, "AntiKT04").jetsByPt(JETPTCUT);                        
      Particles goodelectrons; 
      Jets goodjets;

      selection(electrons, goodelectrons,
		jets, goodjets, Higgs);      

      fillhisto(goodelectrons,
		goodjets,
		Higgs, Nhiggs,
		weight);            
    }




    /// @brief Whether particle p originate from any of the ptcls
    bool originateFrom(const Particle& p, const Particles& ptcls ) {
      const GenVertex* prodVtx = p.genParticle()->production_vertex();
      if (prodVtx == nullptr) return false;
      // for each ancestor, check if it matches any of the input particles
      for (auto ancestor:particles(prodVtx, HepMC::ancestors)){ 
	for ( auto part:ptcls ) 
	  if ( ancestor==part.genParticle() ) return true;
      }
      // if we get here, no ancetor matched any input particle
      return false; 
    }
    
    /// @brief Whether particle p originates from p2
    bool originateFrom(const Particle& p, const Particle& p2 ) { 
      Particles ptcls = {p2}; return originateFrom(p,ptcls);
    }

    bool hasChild(const GenParticle *ptcl, int pdgID) {
      for (auto child:Particle(*ptcl).children())
        if (child.pdgId()==pdgID) return true;
      return false;
    }

    /// @brief Checks whether the input particle has a parent with a given PDGID 
    bool hasParent(const GenParticle *ptcl, int pdgID) {
      for (auto parent:particles(ptcl->production_vertex(),HepMC::parents))
        if (parent->pdg_id()==pdgID) return true;
      return false;
    }



    //===================================================
    //                    Selection
    //===================================================

    void selection(const Particles& electrons, Particles& goodelectrons,
		   const Jets& jets, Jets& goodjets, Particle Higgs) {      

      const double e_pt_cut = 7*GeV;
      const double e_eta_cut = 2.7;
      const double jet_pt_cut = 25*GeV;
      const double jet_eta_cut = 4.5;

      foreach (const Particle& e, electrons) {if (e.pT() > e_pt_cut && fabs(e.eta()) < e_eta_cut)  goodelectrons.push_back(e);}
      
           
      foreach (const Jet& jet, jets) {	
	
	bool NoElectronInsideJet = true;
	foreach (const Particle& e, goodelectrons) {
	  const double deltaRej = deltaR(jet.momentum(), e.momentum());
	  if (deltaRej < 0.4) {NoElectronInsideJet = false;}
	}

	if (NoElectronInsideJet && jet.pT() > jet_pt_cut && fabs(jet.eta()) < jet_eta_cut) goodjets.push_back(jet);
      }

    }
    
  

    //===================================================
    //                  Fill histograms
    //===================================================

    void fillhisto(Particles& goodelectrons,
		   Jets& goodjets, 		   		   
		   Particle Higgs, int Nhiggs,
		   double& weight) {
    
      const int nb_leptons = goodelectrons.size();
      FourMomentum VB_m;
      Jet thirdjet;
      Jet thirdjet_all;


      if (Nhiggs == 1 && nb_leptons == 2) {

	VB_m = goodelectrons[0].momentum() + goodelectrons[1].momentum();
        double deltaPhiVjj = deltaPhi(Higgs, VB_m);
        FourMomentum VH = VB_m + Higgs;	       
	FourMomentum mll = goodelectrons[0].momentum() + goodelectrons[1].momentum();                                                                                                    

      	_h_VpT_inclusive->fill(VB_m.pT()/GeV, weight);
	_h_mll_presel->fill(mll.mass()/GeV, weight);

	if(mll.mass()>81*GeV && mll.mass()<101*GeV) {
	  
	  _h_mll->fill(mll.mass()/GeV, weight);
	  _h_VpT_2L->fill(VB_m.pT()/GeV, weight);
	  _h_HpT_2L->fill(Higgs.pT()/GeV, weight);	  	  
	  _h_njet_2L->fill(goodjets.size(), weight);
	  _h_DeltaPhiVjj_2L->fill(deltaPhiVjj, weight);	  
	  _h_pTVH_2L->fill(VH.pT()/GeV,weight);
	  _h_mVH_2L->fill(VH.mass()/GeV,weight);
	  
	  if(goodjets.size()==0) {
	    _h_VpT_2L_0J->fill(VB_m.pT()/GeV, weight);
	    _h_HpT_2L_0J->fill(Higgs.pT()/GeV, weight);
	    _h_DeltaPhiVjj_2L_0J->fill(deltaPhiVjj, weight);	    
	    _h_pTVH_2L_0J->fill(VH.pT()/GeV,weight);
	    _h_mVH_2L_0J->fill(VH.mass()/GeV,weight);
	    _h_VpT_inclusive_0J->fill(VB_m.pT()/GeV, weight);
	  }
	  
	  else if(goodjets.size()>0) {
	    _h_VpT_2L_1pJ->fill(VB_m.pT()/GeV, weight);
	    _h_HpT_2L_1pJ->fill(Higgs.pT()/GeV, weight);
	    _h_DeltaPhiVjj_2L_1pJ->fill(deltaPhiVjj, weight);	  
	    _h_pTVH_2L_1pJ->fill(VH.pT()/GeV,weight);
	    _h_mVH_2L_1pJ->fill(VH.mass()/GeV,weight);
	    _h_VpT_inclusive_1pJ->fill(VB_m.pT()/GeV, weight);
	    
	    thirdjet = goodjets[0];
	    
	    double deltaPhiVHj=deltaPhi(VH, thirdjet.momentum());
	    _h_DeltaPhiVHj3_2L->fill(deltaPhiVHj,weight);
	    _h_ptj3_2L->fill(thirdjet.pT()/GeV,weight);
	    
	    FourMomentum balancej3 = VH + thirdjet.momentum();
	    _h_ptbalancej3_2L->fill(balancej3.pT()/GeV,weight);
	    if(goodjets.size()==1) _h_ptbalancej3_2L_1J->fill(balancej3.pT()/GeV,weight);
	    if(goodjets.size()>1) _h_ptbalancej3_2L_2pJ->fill(balancej3.pT()/GeV,weight);	    
	  }   
	}	
      }
    }
    
    
    // rescaling of histograms
    void finalize() {
      _h_xsect->fill(0,crossSection());
      _h_xsect->fill(2,sumOfWeights());
      
      scale(_h_VpT_2L, crossSection()/sumOfWeights());
      scale(_h_njet_2L, crossSection()/sumOfWeights());
      scale(_h_HpT_2L, crossSection()/sumOfWeights());
      scale(_h_DeltaPhiVjj_2L, crossSection()/sumOfWeights());
      
      scale(_h_VpT_2L_0J, crossSection()/sumOfWeights());
      scale(_h_HpT_2L_0J, crossSection()/sumOfWeights());
      scale(_h_DeltaPhiVjj_2L_0J, crossSection()/sumOfWeights());
      
      scale(_h_VpT_2L_1pJ, crossSection()/sumOfWeights());
      scale(_h_HpT_2L_1pJ, crossSection()/sumOfWeights());
      scale(_h_DeltaPhiVjj_2L_1pJ, crossSection()/sumOfWeights());
            
      scale(_h_VpT_inclusive, crossSection()/sumOfWeights()); 
      scale(_h_mll_presel, crossSection()/sumOfWeights());
      scale(_h_mll, crossSection()/sumOfWeights());
      
      scale(_h_VpT_inclusive_0J, crossSection()/sumOfWeights());
      scale(_h_mll_presel_0J, crossSection()/sumOfWeights());
      
      scale(_h_VpT_inclusive_1pJ, crossSection()/sumOfWeights());
      scale(_h_mll_presel_1pJ, crossSection()/sumOfWeights());
      
      scale(_h_DeltaPhiVHj3_2L, crossSection()/sumOfWeights());
      scale(_h_ptj3_2L, crossSection()/sumOfWeights());
      scale(_h_ptbalancej3_2L, crossSection()/sumOfWeights());
      scale(_h_ptbalancej3_2L_1J, crossSection()/sumOfWeights());
      scale(_h_ptbalancej3_2L_2pJ, crossSection()/sumOfWeights());
      
      scale(_h_mVH_2L, crossSection()/sumOfWeights());
      scale(_h_mVH_2L_0J, crossSection()/sumOfWeights());
      scale(_h_mVH_2L_1pJ, crossSection()/sumOfWeights());
      scale(_h_pTVH_2L, crossSection()/sumOfWeights());
      scale(_h_pTVH_2L_0J, crossSection()/sumOfWeights());
      scale(_h_pTVH_2L_1pJ, crossSection()/sumOfWeights());
      
  } 
  
  private:
    Histo1DPtr _h_xsect;


    Histo1DPtr _h_pTVH_2L;
    Histo1DPtr _h_mVH_2L;

    Histo1DPtr _h_pTVH_2L_0J;
    Histo1DPtr _h_mVH_2L_0J;

    Histo1DPtr _h_pTVH_2L_1pJ;
    Histo1DPtr _h_mVH_2L_1pJ;
    
    Histo1DPtr _h_VpT_2L;
    Histo1DPtr _h_VpT_2L_0J;
    Histo1DPtr _h_VpT_2L_1pJ;
   
    Histo1DPtr _h_HpT_2L;
    Histo1DPtr _h_HpT_2L_0J;
    Histo1DPtr _h_HpT_2L_1pJ;

    Histo1DPtr _h_njet_2L;

    Histo1DPtr _h_DeltaPhiVjj_2L;
    Histo1DPtr _h_DeltaPhiVjj_2L_0J;
    Histo1DPtr _h_DeltaPhiVjj_2L_1pJ;

    Histo1DPtr _h_DeltaPhiVHj3_2L;
    Histo1DPtr _h_ptj3_2L;
    Histo1DPtr _h_ptbalancej3_2L;
    Histo1DPtr _h_ptbalancej3_2L_1J;
    Histo1DPtr _h_ptbalancej3_2L_2pJ;

    Histo1DPtr _h_VpT_inclusive; 
    Histo1DPtr _h_mll_presel;
    Histo1DPtr _h_mll;

    Histo1DPtr _h_VpT_inclusive_0J;
    Histo1DPtr _h_mll_presel_0J;

    Histo1DPtr _h_VpT_inclusive_1pJ;
    Histo1DPtr _h_mll_presel_1pJ;
    
  };
  
  DECLARE_RIVET_PLUGIN(undecayed_simple_ZllH);
  
}










