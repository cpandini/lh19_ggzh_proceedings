// -*- C++ -*-
#include "Rivet/Jet.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
//#include "Rivet/Tools/RivetBoost.hh"
//#include "Rivet/ParticleName.hh"
#include "Rivet/Particle.hh"
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Math/LorentzTrans.hh"
#include "Rivet/Math/Constants.hh"
#include <cmath>
#include <vector>

namespace Rivet {
    
  using namespace Cuts;
    
  class undecayed_HXSWG_VH_Selection : public Analysis {
  public:
        
    /// Constructor
    undecayed_HXSWG_VH_Selection()
      : Analysis("undecayed_HXSWG_VH_Selection")
    {    }


    ///////////////////////////////////////////////////////////////////
    ///
    ///                    INITIALIZATION FUNCTION
    ///
    ///////////////////////////////////////////////////////////////////
        
    // initialize all the projections
    // book all histograms
        
    void init() {           

      //---------------------------------------------------
      //             Definition of projections
      //---------------------------------------------------
            
      // Set up projections
      FinalState fs;
      addProjection(fs, "FinalState");

      // identified Higgs (only used for Sherpa)
      IdentifiedFinalState hfinder(fs);
      hfinder.acceptIdPair(25);
      declare(hfinder, "HFinder");

      // Cuts for electrons and muons used to build Z and W
      Cut cut_e = etaIn(-2.7,2.7) & (Cuts::pT >= 7*GeV); // Cuts:: has been added because in version 2.7.2 there is another pT variable and can create ambiguities
      Cut cut_m = etaIn(-2.7,2.7) & (Cuts::pT >= 7*GeV); // Cuts:: has been added because in version 2.7.2 there is another pT variable and can create ambiguities
            
      // Gets electrons
      IdentifiedFinalState electrons(cut_e);
      electrons.acceptIdPair(PID::ELECTRON);
      addProjection(electrons, "Electrons");
      // Dressed electrons
      DressedLeptons electronClusters(fs, electrons, 0.1, /*true,*/ cut_e);
      addProjection(electronClusters, "ElectronClusters");
            
      // Gets muons
      IdentifiedFinalState muons(cut_m);
      muons.acceptIdPair(PID::MUON);
      addProjection(muons, "Muons");
      // Dressed muons
      DressedLeptons muonClusters(fs, muons, 0.1, /*true,*/ cut_m);
      addProjection(muonClusters, "MuonClusters");
            
      // Final state used as input for jet-finding.
      FinalState fs_jets(etaIn(-5,5));
      addProjection(fs_jets, "FinalState_jets");
      // Calorimeter particles : all particles but muons and neutrinos
      VetoedFinalState jet_input(fs_jets);
      jet_input.vetoNeutrinos();
      jet_input.addVetoPairId(PID::MUON);
      jet_input.addDecayProductsVeto(PID::HIGGS);
      addProjection(jet_input, "Jet_input");
            
      // Get the jets
      addProjection(FastJets(jet_input, FastJets::ANTIKT, 0.4), "AntiKT04");
            
      // all tracks (to do deltaR with leptons)
      addProjection(ChargedFinalState(-2.5,2.5,0.5*GeV),"cfs");

      // histogram booking
      string name;
      name = "SAMPLE_2lepton_";		
      _h_njet_2L = bookHisto1D(name + "njet", 20, 0, 20);

      _h_VpT_2L = bookHisto1D(name + "pTV", 200, 0, 3000);
      _h_pTVH_2L = bookHisto1D(name + "pTVH", 300, 0, 1500);
      _h_mVH_2L = bookHisto1D(name + "mVH", 200, 0, 1000);            
      _h_HpT_2L = bookHisto1D(name + "pTH", 200, 0, 3000);
      _h_DeltaPhiVjj_2L = bookHisto1D(name + "DeltaPhiVjj", 50, 0, 3.15);	  	  
      
      _h_VpT_2L_0J = bookHisto1D(name + "0J_pTV", 200, 0, 3000);
      _h_pTVH_2L_0J = bookHisto1D(name + "0J_pTVH", 300, 0, 1500);
      _h_mVH_2L_0J = bookHisto1D(name + "0J_mVH", 200, 0, 1000);
      _h_HpT_2L_0J = bookHisto1D(name + "0J_pTH", 200, 0, 3000);
      _h_DeltaPhiVjj_2L_0J = bookHisto1D(name + "0J_DeltaPhiVjj", 50, 0, 3.15);      
     
      _h_VpT_2L_1pJ = bookHisto1D(name + "1pJ_pTV", 200, 0, 3000);
      _h_pTVH_2L_1pJ = bookHisto1D(name + "1pJ_pTVH", 300, 0, 1500);
      _h_mVH_2L_1pJ = bookHisto1D(name + "1pJ_mVH", 200, 0, 1000);
      _h_HpT_2L_1pJ = bookHisto1D(name + "1pJ_pTH", 200, 0, 3000);
      _h_DeltaPhiVjj_2L_1pJ = bookHisto1D(name + "1pJ_DeltaPhiVjj", 50, 0, 3.15);
      
      _h_DeltaPhiVHj3_2L = bookHisto1D(name + "DeltaPhiVHj3", 50, 0, 3.15);	  	  
      _h_ptj3_2L = bookHisto1D(name + "ptj3", 100, 0, 1000);
      _h_ptj3_all_2L = bookHisto1D(name + "ptj3_all", 100, 0, 1000);
      _h_ptbalancej3_2L = bookHisto1D(name + "ptbalancej3", 100, 0, 1000);
      _h_ptbalancej3_2L_1J = bookHisto1D(name + "ptbalancej3_1J", 100, 0, 1000);
      _h_ptbalancej3_2L_2pJ = bookHisto1D(name + "ptbalancej3_2pJ", 100, 0, 1000);
           
      _h_VpT_inclusive = bookHisto1D("pTV_inclusive", 200, 0, 2000);      
      _h_VpT_inclusive_mll = bookHisto1D("pTV_inclusive_mll", 200, 0, 2000);
      _h_mll_presel = bookHisto1D("mll_presel", 100, 0, 300);

      _h_VpT_inclusive_0J = bookHisto1D("pTV_inclusive_0J", 200, 0, 2000);
      _h_VpT_inclusive_mll_0J = bookHisto1D("pTV_inclusive_mll_0J", 200, 0, 2000);
      _h_mll_presel_0J = bookHisto1D("mll_presel_0J", 100, 0, 300);

      _h_VpT_inclusive_1pJ = bookHisto1D("pTV_inclusive_1pJ", 200, 0, 2000);
      _h_VpT_inclusive_mll_1pJ = bookHisto1D("pTV_inclusive_mll_1pJ", 200, 0, 2000);
      _h_mll_presel_1pJ = bookHisto1D("mll_presel_1pJ", 100, 0, 300);

      _h_xsect = bookHisto1D("CrossSection",3,-0.5,2.5);
      
    }
    


    ///////////////////////////////////////////////////////////////////
    ///
    ///                       ANALYSIS FUNCTION
    ///
    ///////////////////////////////////////////////////////////////////
      
    // do per-event analysis
      
    void analyze(const Event& event) {
      double weight = event.weight();
      _h_xsect->fill(1,weight);
	


      // Find undecayed Higgs particle
      Particle Higgs;
      GenVertex *HSvtx = event.genEvent()->signal_process_vertex();
      int Nhiggs=0;
      for ( const GenParticle *ptcl : Rivet::particles(event.genEvent()) ) {

        // a) Reject all non-Higgs particles
	if ( !PID::isHiggs(ptcl->pdg_id()) ) continue;
        // b) select only the final Higgs boson copy, prior to decay
        if ( ptcl->end_vertex() && !hasChild(ptcl,PID::HIGGS) ) {
	  Higgs = ptcl; ++Nhiggs;
        }
        // c) if HepMC::signal_proces_vertex is missing
	//    set hard-scatter vertex based on first Higgs boson
        if ( HSvtx==nullptr && ptcl->production_vertex() && !hasParent(ptcl,PID::HIGGS) )
          HSvtx = ptcl->production_vertex();
      }

      if (HSvtx == nullptr || Nhiggs != 1) {
        // assume we are analysing Sherpa events
        const IdentifiedFinalState& hfinder = apply<IdentifiedFinalState>(event, "HFinder");
        Particles hbosons = hfinder.particles();
        Nhiggs = hbosons.size();
        if (Nhiggs!=1) {
          vetoEvent;
        }
        Higgs = hbosons[0];
      } else {
        if (HSvtx == nullptr) vetoEvent;
        if (Nhiggs!=1) vetoEvent;
      }
      
      //---------------------------------------------------
      //             Creation of projections
      //---------------------------------------------------
            
      // Get the electrons (ordered by pT)
      const Particles& electrons = applyProjection<FinalState>(event, "Electrons").particlesByPt();
      const DressedLeptons& electronClusters = applyProjection<DressedLeptons>(event, "ElectronClusters");
      const Particles& muons = applyProjection<FinalState>(event, "Muons").particlesByPt();
      const DressedLeptons& muonClusters = applyProjection<DressedLeptons>(event, "MuonClusters");            
      // Tracks
      const Particles chg_tracks = applyProjection<ChargedFinalState>(event, "cfs").particlesByPt();            
      // Calorimeter particles
      const Particles calo_part = applyProjection<VetoedFinalState>(event, "Jet_input").particlesByPt();            
      // Visible particles (for Et miss)
      const Particles vis_part = calo_part + muons;            
      // Get the jets (ordered by pT)
      const double JETPTCUT = 10*GeV;// ATLAS standard jets collection
      const Jets jets = applyProjection<FastJets>(event, "AntiKT04").jetsByPt(JETPTCUT);
                        
      // List of all physical objects passing cuts
      Particles vectorBosons;
      Particles goodelectrons; //electrons passing cuts
      Particles muons_cut, goodmuons; //muons passing cuts
      Particles electrons_T, electrons_L_not_T, electrons_cut, electrons_with_muons;
      Particles muons_T, muons_L_not_T;
      Particles chosenleptons;
      Jets additionaljets;
      Jets goodjets;
      Jets signaljets;
      int nb_leptons; nb_leptons = -5;	    

      //---------------------------------------------------                                                                                                                                                                       
      //                     Analysis                                                                                                                                                                                             
      //---------------------------------------------------                                                                                                                                                                       

      selection(chg_tracks, calo_part,
		electrons, muons,
		electrons_with_muons, electrons_cut, goodelectrons,
		electrons_T, electrons_L_not_T,
		muons_cut, goodmuons,
		muons_T, muons_L_not_T,
		jets, goodjets, signaljets, additionaljets, Higgs);
      
      characterization(vis_part,
		       electrons_cut, electrons_T, electrons_L_not_T,
		       muons_cut, muons_T, muons_L_not_T,
		       goodjets, additionaljets, signaljets,
		       chosenleptons,
		       nb_leptons);
      
      fillhisto(vis_part, chg_tracks,
		electrons, muons,
		goodelectrons, goodmuons,
		goodjets, additionaljets, signaljets,
		chosenleptons,
		nb_leptons,
		Higgs, Nhiggs,
		weight);
      
      
    }

    /// @brief Whether particle p originate from any of the ptcls
    bool originateFrom(const Particle& p, const Particles& ptcls ) {
      const GenVertex* prodVtx = p.genParticle()->production_vertex();
      if (prodVtx == nullptr) return false;
      // for each ancestor, check if it matches any of the input particles
      for (auto ancestor:particles(prodVtx, HepMC::ancestors)){ 
	for ( auto part:ptcls ) 
	  if ( ancestor==part.genParticle() ) return true;
      }
      // if we get here, no ancetor matched any input particle
      return false; 
    }
    
    /// @brief Whether particle p originates from p2
    bool originateFrom(const Particle& p, const Particle& p2 ) { 
      Particles ptcls = {p2}; return originateFrom(p,ptcls);
    }

    bool hasChild(const GenParticle *ptcl, int pdgID) {
      for (auto child:Particle(*ptcl).children())
        if (child.pdgId()==pdgID) return true;
      return false;
    }

    /// @brief Checks whether the input particle has a parent with a given PDGID 
    bool hasParent(const GenParticle *ptcl, int pdgID) {
      for (auto parent:particles(ptcl->production_vertex(),HepMC::parents))
        if (parent->pdg_id()==pdgID) return true;
      return false;
    }



    //===================================================
    //                    Selection
    //===================================================

    void selection(const Particles& chg_tracks, const Particles& calo_part,
		   const Particles& electrons, const Particles& muons,
		   Particles& electrons_with_muons, Particles& electrons_cut, Particles& goodelectrons,
		   Particles& electrons_T, Particles& electrons_L_not_T,
		   Particles& muons_cut, Particles& goodmuons,
		   Particles& muons_T, Particles& muons_L_not_T,
		   const Jets& jets, Jets& goodjets, Jets& signaljets, Jets& additionaljets, Particle Higgs) {
      
      // Definition of all the cuts applied
      // to electrons
      const double e_pt_cut_loose = 7*GeV;
      const double e_eta_cut_loose = 2.7;
      const double e_iso_track_cut_loose = 0.1;
      const double e_pt_cut_tight = 25*GeV;
      const double e_eta_cut_tight = 2.7;
      // to muons
      const double m_pt_cut_loose = 7*GeV;
      const double m_eta_cut_loose = 2.7;
      const double m_iso_track_cut_loose = 0.1;
      const double m_pt_cut_tight = 25*GeV;
      const double m_eta_cut_tight = 2.5;

      // to central jets
      const double centraljet_pt_cut = 20*GeV;
      const double centraljet_eta_cut = 2.5;
      // to forward jets
      const double additional_jet_pt_cut = 30*GeV;
      const double additional_jet_eta_cut = 4.5;

      ///// Selection of lepton passing at least the loose criteria
      //
      double sum_pt_tracks;

      // muons
      foreach (const Particle& m, muons) {
	sum_pt_tracks = 0;
	foreach (const Particle& c, chg_tracks) { if ( deltaR(m.momentum(), c.momentum()) < 0.2) { sum_pt_tracks += c.pT(); } }
	sum_pt_tracks -= m.pT();
	if (m.pT() > m_pt_cut_loose && // Loose cut
	    fabs(m.eta()) < m_eta_cut_loose &&
	    (sum_pt_tracks/(m.pT())) < m_iso_track_cut_loose) { goodmuons.push_back(m); }
	else{muons_cut.push_back(m);}
      }

      // electrons
      foreach (const Particle& e, electrons) {
	sum_pt_tracks = 0;
	foreach (const Particle& c, chg_tracks) { if ( deltaR(e.momentum(), c.momentum()) < 0.2) { sum_pt_tracks += c.pT(); } }
	sum_pt_tracks -= e.pT();
	if (e.pT() > e_pt_cut_loose && // Loose cut
	    fabs(e.eta()) < e_eta_cut_loose &&
	    (sum_pt_tracks/(e.pT())) < e_iso_track_cut_loose) { goodelectrons.push_back(e); }
	else{electrons_cut.push_back(e);}
      }

      //Overlap removal : electron/muons
      Particles tmp_goodelectrons;
      bool NoMuonInside = 1;
      foreach (const Particle& ge, goodelectrons) {
	NoMuonInside = 1;
	foreach (const Particle& m, goodmuons) {
	  const double deltaRem = deltaR(ge.momentum(), m.momentum());
	  if (deltaRem < 0.2) {NoMuonInside = 0;}
	}
	if (NoMuonInside) {
	  tmp_goodelectrons.push_back(ge);
	}
	else if (!NoMuonInside){
	  electrons_with_muons.push_back(ge);
	}
      }
      goodelectrons.clear();
      goodelectrons = tmp_goodelectrons;

      //Quality of electrons
      foreach (const Particle& e, goodelectrons) {
	if (e.pT() > e_pt_cut_tight && // Tight cut
	    fabs(e.eta()) < e_eta_cut_tight ) electrons_T.push_back(e);
	else electrons_L_not_T.push_back(e);
      }

      //Quality of muons
      foreach (const Particle& m, goodmuons) {
	if (m.pT() > m_pt_cut_tight && // Tight cut
	    fabs(m.eta()) < m_eta_cut_tight ) muons_T.push_back(m);
	else muons_L_not_T.push_back(m);
      }

      bool NoElectronInsideJet = 1; // equal to 1 if there is no lepton in jet, equal to 0 if there is lepton in jet
      bool NoMuonInsideJet = 1;

      // Leptons-jets overlap removal, using Loose leptons
      foreach (const Jet& jet, jets) {
	
	NoElectronInsideJet = 1;
	foreach (const Particle& e, goodelectrons) { // Overlap removal : jet/electrons
	  const double deltaRej = deltaR(jet.momentum(), e.momentum());
	  if (deltaRej < 0.4) {NoElectronInsideJet = 0;}
	}	                                                                                                                                                                                          
	NoMuonInsideJet = 1;
	foreach (const Particle& m, goodmuons) { // Overlap removal : jet/muons
	  const double deltaRmj = deltaR(jet.momentum(), m.momentum());
	  if (deltaRmj < 0.4) {
	    HepMC::GenVertex* mvertex = m.genParticle()->production_vertex();
	    int Ntrack = 0;
	    foreach (const Particle& c, chg_tracks) {
	      HepMC::GenVertex* cvertex = c.genParticle()->production_vertex();
	      if (cvertex == mvertex) {Ntrack = Ntrack + 1;}
	    }
	    if (Ntrack <= 3) {NoMuonInsideJet = 0;}
	  }
	}

	// Check if b-tagged and cuts
	if (NoElectronInsideJet && NoMuonInsideJet) {

	  // goodjets = collection of central+forward jets
	  if ((jet.pT() > centraljet_pt_cut && fabs(jet.eta())< centraljet_eta_cut) || // pT>20GeV && |eta|<2.5 [central jets]                                                                                                  
	      (jet.pT() > additional_jet_pt_cut && fabs(jet.eta()) < additional_jet_eta_cut)) { // pT>30GeV && |eta|<4.5 [forward jets]                                                                                         
	    goodjets.push_back(jet);

	    if (jet.pT() > centraljet_pt_cut && fabs(jet.eta())< centraljet_eta_cut) signaljets.push_back(jet);
	    else additionaljets.push_back(jet);
	  }
	}
      }
    }
    
  
    //===================================================                                                                                                                                                                           
    //         Characterization of the event                                                                                                                                                                                        
    //===================================================                                                                                                                                                                           

    void characterization(const Particles& vis_part,
			  Particles& electrons_cut, Particles& electrons_T, Particles& electrons_L_not_T,
			  Particles& muons_cut, Particles& muons_T, Particles& muons_L_not_T,
			  Jets& goodjets, Jets& additionaljets, Jets& signaljets,
			  Particles& chosenleptons,
			  int& nb_leptons) {

      //Classification of event type

      //0-leptons, 1-leptons or dileptons state
      const size_t ne_T = electrons_T.size();
      const size_t ne_L = electrons_L_not_T.size();
      const size_t nm_T = muons_T.size();
      const size_t nm_L = muons_L_not_T.size();

      // Et miss
      FourMomentum eTmiss_m;
      foreach ( const Particle& p, vis_part ) {	eTmiss_m -= p.momentum(); }
      const double eTmiss = eTmiss_m.pT();

      // 0-leptons
      if ( (ne_T + ne_L + nm_T + nm_L) == 0) { nb_leptons = 0; } // veto loose leptons (el,mu)

      // 1-leptons
      else if ( (ne_L + nm_T + nm_L) == 0 && ne_T == 1) { // exactly 1 tight electron
	if(eTmiss > 30*GeV) { // MET cut 30GeV (for the electron channel only)
	  nb_leptons = 1;
	  chosenleptons.push_back(electrons_T[0]);
	} else { nb_leptons = -1; }
      }
      else if ( (ne_T + ne_L + nm_L) == 0 && nm_T == 1) { // exactly 1 tight muon
	nb_leptons = 1;
	chosenleptons.push_back(muons_T[0]);
      } 
      else if (ne_T + ne_L + nm_L + nm_T == 1) { // any other combinations of leptons
	nb_leptons = -1;
      }

      // 2-leptons
      else if ( ne_T == 1 && ne_L == 1 &&  (nm_T + nm_L) == 0 ) { // exactly 1 tight electron + 1 loose electron, veto loose muons
	chosenleptons.push_back(electrons_T[0]);
	chosenleptons.push_back(electrons_L_not_T[0]);
	if(chosenleptons.size()>1) nb_leptons = 2;
      }
      else if ( nm_T == 1 && nm_L == 1 &&  (ne_T + ne_L) == 0 ) { // exactly 1 tight muon + 1 loose muon, veto loose electrons
	chosenleptons.push_back(muons_T[0]);
	chosenleptons.push_back(muons_L_not_T[0]);
	if(chosenleptons.size()>1) {
	  if(chosenleptons[0].charge()*chosenleptons[1].charge()<0) nb_leptons = 2;
	}
      }      
      else if ( (ne_L + nm_T + nm_L) == 0 && ne_T == 2) { // exactly 2 tight electrons, veto on loose muons
	chosenleptons.push_back(electrons_T[0]);
	chosenleptons.push_back(electrons_T[1]);
	if(chosenleptons.size()>1) nb_leptons = 2;
      }
      else if ( (ne_T + ne_L + nm_L) == 0 && nm_T == 2) { // exactly 2 tight muons, veto on loose electrons
	chosenleptons.push_back(muons_T[0]);
	chosenleptons.push_back(muons_T[1]);
	if(chosenleptons.size()>1) {
	  if(chosenleptons[0].charge()*chosenleptons[1].charge()<0) nb_leptons = 2;
	}
      }
      
      else {
	nb_leptons = -3;
      }
    }

    //===================================================
    //                  Fill histograms
    //===================================================

    void fillhisto(const Particles& vis_part, const Particles& chg_tracks,
		   const Particles& electrons, const Particles& muons,
		   Particles& goodelectrons, Particles& goodmuons,
		   Jets& goodjets, Jets& additionaljets, Jets& signaljets,
		   Particles& chosenleptons,
		   int& nb_l,
		   Particle Higgs, int Nhiggs,
		   double& weight) {
    
      const int nb_leptons = nb_l;

      FourMomentum pTmiss;
      foreach ( const Particle& c, chg_tracks ) {pTmiss -= c.momentum();}
      FourMomentum eTmiss_m;
      foreach ( const Particle& p, vis_part ) {eTmiss_m -= p.momentum();}
      const double eTmiss = eTmiss_m.pT();
      FourMomentum VB_m;
      FourMomentum eTmiss_T;
      eTmiss_T=eTmiss_m;
      eTmiss_T.setPz(0);
      Jet thirdjet;
      Jet thirdjet_all;

      if (Nhiggs!=1) vetoEvent;                  
      if (nb_leptons == 2){                                                                                                                                                                        

	VB_m = chosenleptons[0].momentum() + chosenleptons[1].momentum();

        double deltaPhiVjj = deltaPhi(Higgs, VB_m);
        FourMomentum VH = VB_m + Higgs;	       




      	_h_VpT_inclusive->fill(VB_m.pT()/GeV, weight);                                                                                                                                           
	if(goodjets.size()==0) _h_VpT_inclusive_0J->fill(VB_m.pT()/GeV, weight);
	if(goodjets.size()>0) _h_VpT_inclusive_1pJ->fill(VB_m.pT()/GeV, weight);
	
	FourMomentum mll = chosenleptons[0].momentum() + chosenleptons[1].momentum();                                                                                                    
	_h_mll_presel->fill(mll.mass()/GeV, weight);
	if(goodjets.size()==0) _h_mll_presel_0J->fill(mll.mass()/GeV, weight);
	if(goodjets.size()>0) _h_mll_presel_1pJ->fill(mll.mass()/GeV, weight);

	if(mll.mass()>81*GeV && mll.mass()<101*GeV) { 
	  _h_VpT_inclusive_mll->fill(VB_m.pT()/GeV, weight);      
	  _h_VpT_2L->fill(VB_m.pT()/GeV, weight);
	  _h_HpT_2L->fill(Higgs.pT()/GeV, weight);	  	  
	  _h_njet_2L->fill(goodjets.size(), weight);
	  _h_DeltaPhiVjj_2L->fill(deltaPhiVjj, weight);	  
	  _h_pTVH_2L->fill(VH.pT()/GeV,weight);
	  _h_mVH_2L->fill(VH.mass()/GeV,weight);
	  
	  if(goodjets.size()==0) {
	    _h_VpT_inclusive_mll_0J->fill(VB_m.pT()/GeV, weight);
	    _h_VpT_2L_0J->fill(VB_m.pT()/GeV, weight);
	    _h_HpT_2L_0J->fill(Higgs.pT()/GeV, weight);
	    _h_DeltaPhiVjj_2L_0J->fill(deltaPhiVjj, weight);	    
	    _h_pTVH_2L_0J->fill(VH.pT()/GeV,weight);
	    _h_mVH_2L_0J->fill(VH.mass()/GeV,weight);
	  }
	  
	  else if(goodjets.size()>0) {
	    _h_VpT_inclusive_mll_1pJ->fill(VB_m.pT()/GeV, weight);	       
	    _h_VpT_2L_1pJ->fill(VB_m.pT()/GeV, weight);
	    _h_HpT_2L_1pJ->fill(Higgs.pT()/GeV, weight);
	    _h_DeltaPhiVjj_2L_1pJ->fill(deltaPhiVjj, weight);	  
	    _h_pTVH_2L_1pJ->fill(VH.pT()/GeV,weight);
	    _h_mVH_2L_1pJ->fill(VH.mass()/GeV,weight);
	    	    
	    if (signaljets.size()>0) thirdjet = signaljets[0];
	    else if (goodjets.size()>0) thirdjet = additionaljets[0];
	    thirdjet_all = goodjets[0];
	    
	    double deltaPhiVHj=deltaPhi(VH, thirdjet.momentum());
	    _h_DeltaPhiVHj3_2L->fill(deltaPhiVHj,weight);
	    _h_ptj3_2L->fill(thirdjet.pT()/GeV,weight);
	    _h_ptj3_all_2L->fill(thirdjet_all.pT()/GeV,weight);

	    FourMomentum balancej3 = VH + thirdjet.momentum();
	    _h_ptbalancej3_2L->fill(balancej3.pT()/GeV,weight);
	    if(goodjets.size()==1) _h_ptbalancej3_2L_1J->fill(balancej3.pT()/GeV,weight);
	    if(goodjets.size()>1) _h_ptbalancej3_2L_2pJ->fill(balancej3.pT()/GeV,weight);
	  }
	}   	
      }
    }
    
    
    // rescaling of histograms
    void finalize() {
      _h_xsect->fill(0,crossSection());
      _h_xsect->fill(2,sumOfWeights());
      
      scale(_h_VpT_2L, crossSection()/sumOfWeights());
      scale(_h_njet_2L, crossSection()/sumOfWeights());
      scale(_h_HpT_2L, crossSection()/sumOfWeights());
      scale(_h_DeltaPhiVjj_2L, crossSection()/sumOfWeights());
      
      scale(_h_VpT_2L_0J, crossSection()/sumOfWeights());
      scale(_h_HpT_2L_0J, crossSection()/sumOfWeights());
      scale(_h_DeltaPhiVjj_2L_0J, crossSection()/sumOfWeights());
      
      scale(_h_VpT_2L_1pJ, crossSection()/sumOfWeights());
      scale(_h_HpT_2L_1pJ, crossSection()/sumOfWeights());
      scale(_h_DeltaPhiVjj_2L_1pJ, crossSection()/sumOfWeights());
            
      scale(_h_VpT_inclusive, crossSection()/sumOfWeights()); 
      scale(_h_VpT_inclusive_mll, crossSection()/sumOfWeights());
      scale(_h_mll_presel, crossSection()/sumOfWeights());
      
      scale(_h_VpT_inclusive_0J, crossSection()/sumOfWeights());
      scale(_h_VpT_inclusive_mll_0J, crossSection()/sumOfWeights());
      scale(_h_mll_presel_0J, crossSection()/sumOfWeights());
      
      scale(_h_VpT_inclusive_1pJ, crossSection()/sumOfWeights());
      scale(_h_VpT_inclusive_mll_1pJ, crossSection()/sumOfWeights());
      scale(_h_mll_presel_1pJ, crossSection()/sumOfWeights());
      
      scale(_h_DeltaPhiVHj3_2L, crossSection()/sumOfWeights());
      scale(_h_ptj3_2L, crossSection()/sumOfWeights());
      scale(_h_ptj3_all_2L, crossSection()/sumOfWeights());
      scale(_h_ptbalancej3_2L, crossSection()/sumOfWeights());
      scale(_h_ptbalancej3_2L_1J, crossSection()/sumOfWeights());
      scale(_h_ptbalancej3_2L_2pJ, crossSection()/sumOfWeights());
      
      scale(_h_mVH_2L, crossSection()/sumOfWeights());
      scale(_h_mVH_2L_0J, crossSection()/sumOfWeights());
      scale(_h_mVH_2L_1pJ, crossSection()/sumOfWeights());
      scale(_h_pTVH_2L, crossSection()/sumOfWeights());
      scale(_h_pTVH_2L_0J, crossSection()/sumOfWeights());
      scale(_h_pTVH_2L_1pJ, crossSection()/sumOfWeights());
      
  } 
  
  private:
    Histo1DPtr _h_xsect;


    Histo1DPtr _h_pTVH_2L;
    Histo1DPtr _h_mVH_2L;

    Histo1DPtr _h_pTVH_2L_0J;
    Histo1DPtr _h_mVH_2L_0J;

    Histo1DPtr _h_pTVH_2L_1pJ;
    Histo1DPtr _h_mVH_2L_1pJ;
    
    Histo1DPtr _h_VpT_2L;
    Histo1DPtr _h_VpT_2L_0J;
    Histo1DPtr _h_VpT_2L_1pJ;
   
    Histo1DPtr _h_HpT_2L;
    Histo1DPtr _h_HpT_2L_0J;
    Histo1DPtr _h_HpT_2L_1pJ;

    Histo1DPtr _h_njet_2L;

    Histo1DPtr _h_DeltaPhiVjj_2L;
    Histo1DPtr _h_DeltaPhiVjj_2L_0J;
    Histo1DPtr _h_DeltaPhiVjj_2L_1pJ;

    Histo1DPtr _h_DeltaPhiVHj3_2L;
    Histo1DPtr _h_ptj3_2L;
    Histo1DPtr _h_ptj3_all_2L;
    Histo1DPtr _h_ptbalancej3_2L;
    Histo1DPtr _h_ptbalancej3_2L_1J;
    Histo1DPtr _h_ptbalancej3_2L_2pJ;

    Histo1DPtr _h_VpT_inclusive; 
    Histo1DPtr _h_VpT_inclusive_mll;
    Histo1DPtr _h_mll_presel;

    Histo1DPtr _h_VpT_inclusive_0J;
    Histo1DPtr _h_VpT_inclusive_mll_0J;
    Histo1DPtr _h_mll_presel_0J;

    Histo1DPtr _h_VpT_inclusive_1pJ;
    Histo1DPtr _h_VpT_inclusive_mll_1pJ;
    Histo1DPtr _h_mll_presel_1pJ;
    
  };
  
  DECLARE_RIVET_PLUGIN(undecayed_HXSWG_VH_Selection);
  
}










