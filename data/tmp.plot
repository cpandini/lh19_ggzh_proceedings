# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_njet
Title = Numbers of jets
YLabel = $\sigma_{N_{\text{jets}}}$ [pb]
XLabel = $N_{\text{jets}}$
XMax=6
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_pTV
Title = $Z$ transverse momentum
XLabel = $p_{\perp}^{Z}$ [GeV]
YLabel = $\text{d}\sigma/\text{d} p_{\perp}^{Z}$ [pb]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_pTVH
Title = $ZH$ system transverse momentum
YLabel = $\text{d}\sigma/\text{d} p_{\perp}^{ZH}$ [pb]
XLabel = $p_{\perp}^{ZH}$ [GeV]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_mVH
Title = Invariant mass distribution
YLabel = $\text{d}\sigma/\text{d} m_{ZH}$ [pb]
XLabel = $m_{ZH}$ [GeV]
XMin = 215
XMax = 1000
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_pTH
Title = Higgs transverse momentum
YLabel = $\text{d}\sigma/\text{d} p_{\perp}^{H}$ [pb]
XLabel = $p_{\perp}^{H}$ [GeV]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_DeltaPhiVjj
Title = $ZH$ angle separation
YLabel = $\text{d}\sigma/\text{d} \Delta \phi (Z,H)$ [pb]
XLabel = $\Delta \phi (Z,H)$
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_0J_pTV
Title = $Z$ transverse momentum with no extra-emissions
YLabel = $\text{d}\sigma/\text{d} p_{\perp Z}$ [pb]
XLabel = $p_{\perp Z}$ [GeV]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_0J_pTVH
Title = $ZH$ system transverse momentum with no extra-emissions
YLabel = $\text{d}\sigma/\text{d} p_{\perp ZH}$ [pb]
XLabel = $p_{\perp ZH}$ [GeV]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_0J_mVH
Title = Invariant mass distribution with no extra-emissions
YLabel = $\text{d}\sigma/\text{d} m_{ZH}$ [pb]
XLabel = $m_{ZH}$ [GeV]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_0J_pTH
Title = Higgs transverse momentum with no extra-emissions
YLabel = $\text{d}\sigma/\text{d} p_{\perp H}$ [pb]
XLabel = $p_{\perp H}$ [GeV]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_0J_DeltaPhiVjj
Title = $ZH$ angle separation with no extra-emissions
YLabel = $\text{d}\sigma/\text{d} \Delta \Phi_{ZH}$ [pb]
XLabel = $\Delta \Phi_{ZH}$
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_1pJ_pTV
Title = $Z$ transverse momentum with at least 1 jet
YLabel = $\text{d}\sigma/\text{d} p_{\perp Z}$ [pb]
XLabel = $p_{\perp Z}$ [GeV]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_1pJ_pTVH
Title = $Z$ transverse momentum with at least 1 jet
YLabel = $\text{d}\sigma/\text{d} p_{\perp Z}$ [pb]
XLabel = $p_{\perp Z}$ [GeV]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_1pJ_mVH
Title = Invariant mass distribution with at least 1 jet
YLabel = $\text{d}\sigma/\text{d} m_{ZH}$ [pb]
XLabel = $m_{ZH}$ [GeV]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_1pJ_pTH
Title = Higgs transverse momentum with at least 1 jet
YLabel = $\text{d}\sigma/\text{d} p_{\perp H}$ [pb]
XLabel = $p_{\perp H}$ [GeV]
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_1pJ_DeltaPhiVjj
Title = $ZH$ angle separation with at least 1 jet
YLabel = $\text{d}\sigma/\text{d} \Delta \Phi_{ZH}$ [pb]
XLabel = $\Delta \Phi_{ZH}$
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_DeltaPhiVHj3
Title = $ZH$-Hardest jet angular separation 
YLabel = $\text{d}\sigma/\text{d} \Delta \Phi_{ZHj}$ [pb]
XLabel = $\Delta \Phi_{ZHj}$
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_ptj3
Title = Hardest jet transverse momentum
YLabel = $\text{d}\sigma/\text{d} p_{\perp}^{j}$ [pb]
XLabel = $p_{\perp}^{j}$ GeV
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_ptbalancej3
Title = $ZHj$ system transverse momentum
YLabel = $\text{d}\sigma/\text{d} p_{\perp ZHj}$ [pb]
XLabel = $p_{\perp ZHj}$ GeV
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_ptbalancej3_1J
Title = $ZHj$ system transverse momentum with no extra-emissions
YLabel = $\text{d}\sigma/\text{d} p_{\perp ZHj}$ [pb]
XLabel = $p_{\perp ZHj}$ GeV
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_ptbalancej3_2pJ
Title = $ZHj$ system transverse momentum with at least 1 jet
YLabel = $\text{d}\sigma/\text{d} p_{\perp ZHj}$ [pb]
XLabel = $p_{\perp ZHj}$ GeV
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/SAMPLE_2lepton_pTV_inclusive
Title = $Z$ inclusive transverse momentum 
YLabel = $\text{d}\sigma/\text{d} p_{\perp Z}$ [pb]
XLabel = $p_{\perp Z}$ GeV
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/mll_presel
Title = $Z$ mass distribution preselection
YLabel = $\text{d}\sigma/\text{d} m_{Z}$ [pb]
XLabel = $m_{Z}$ GeV
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/pTV_inclusive_0j
Title = $Z$ inclusive transverse momentum with no extra emissions
YLabel = $\text{d}\sigma/\text{d} p_{\perp Z}$ [pb]
XLabel = $p_{\perp Z}$ GeV
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/mll_presel_0J
Title = $Z$ mass distribution preselection with 0 J
YLabel = $\text{d}\sigma/\text{d} m_{Z}$ [pb]
XLabel = $m_{Z}$ GeV
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/pTV_inclusive_1pJ
Title = $Z$ inclusive transverse momentum with at least 1 jet
YLabel = $\text{d}\sigma/\text{d} p_{\perp Z}$ [pb]
XLabel = $p_{\perp Z}$ GeV
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/mll_presel_1pJ
Title = $Z$ mass distribution preselection with 1 J
YLabel = $\text{d}\sigma/\text{d} m_{Z}$ [pb]
XLabel = $m_{Z}$ GeV
# END PLOT

# BEGIN PLOT /undecayed_simple_ZllH/CrossSection
Title = Total XS
# END PLOT
