#!/bin/bash
export PATH=".:$PATH"
export RIVET_ANALYSIS_PATH="$RIVET_ANALYSIS_PATH:../rivet-code"

python pylot.py

sed -ie 's/LINE_1/sherpa_lops_res_scalevar/g' undecayed_simple_config.plot
sed -ie 's/LINE_2/Rivet_undecayed_simple_mllcut_main31_200215_0822/g' undecayed_simple_config.plot
sed -ie 's/LINE_3/Rivet_undecayed_simple_mllcut_200214_1547_mllcut/g' undecayed_simple_config.plot

sherpa_scale=0.9561177078  # 0.001852/0.001937 to account for different EW params

rivet-mkhtml --mc-errs --font times \
    sherpa_lops_res_scalevar.yoda:Scale=$sherpa_scale:Title='Sherpa~LO+PS~0-jet':ErrorBands=1:ErrorBars=0:'ErrorBandColor={[RGB]{30,144,255}}':'ErrorBandOpacity=0.3':'LineColor={[RGB]{0,0,255}}':LegendOrder=0 \
    sherpa_lops.yoda:Scale=$sherpa_scale:Title='Sherpa~LO+PS':'LineColor={[RGB]{0,0,255}}' \
    Rivet_undecayed_simple_mllcut_200214_1547_mllcut.yoda:Title='POWHEG+Pythia8~wimpy':'LineColor={[RGB]{34,139,34}}':LegendOrder=1 \
    Rivet_undecayed_simple_mllcut_main31_200215_0822.yoda:Title='POWHEG+Pythia8~vetoed~power':'LineColor={[RGB]{138,0,0}}':LegendOrder=2 \
    -c undecayed_simple_config.plot -o ../plots/showers
